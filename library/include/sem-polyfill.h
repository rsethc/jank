/*
    Code in this file is adapted from:
        https://stackoverflow.com/a/4793662/1917534

    License info:
        https://creativecommons.org/licenses/by-sa/4.0/

    Changes made to this from the original:
        Separated the sample code into header and source files.
        Applied superficial code styling changes.
        Implemented acquire/release of arbitrary amounts instead of always 1.
        Constructor for specifying an arbitrary initial count.

    This file is licensed under the license linked in
        this comment, rather than the MIT license in this repository.
*/

#pragma once

#include <mutex>
#include <condition_variable>

class semaphore
{
protected:
    std::mutex mutex_;
    std::condition_variable condition_;
    unsigned long count_;

public:
    semaphore (int initial);
    void release ();
    void acquire ();
    bool try_acquire ();
};

class semaphore_amounts : private semaphore
{
public:
    using semaphore::semaphore;
    void release (int amount);
    void acquire (int amount);
    bool try_acquire (int amount);
};
