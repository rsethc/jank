#pragma once

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <unordered_map>
#include <thread>
#include <atomic>

//#include <semaphore>
#include "sem-polyfill.h"

#define SEMICOLONS_OPTIONAL false

enum
{
    VALUE_UNKNOWNTYPE,
    VALUE_NOTHING,
    VALUE_INT,
    VALUE_FLOAT,
    VALUE_STR,
    VALUE_CFUNC,
    VALUE_SCRIPTFUNC,
    VALUE_METHODCALL,
    VALUE_ARR,
    VALUE_OBJ,
    VALUE_THREAD,
    VALUE_SEM,

    VALUE_USERDEFINED_BEGIN,
};

enum
{
    UNWIND_NONE = 0,

    UNWIND_RETURN,
    UNWIND_THROW,
    UNWIND_BREAK,
    UNWIND_CONTINUE,
};

struct VM;
struct Assignable;
struct Value
{
    int type = VALUE_UNKNOWNTYPE;
    virtual ~Value ();
    virtual Value* Clone () = 0;
    virtual std::string ToString () = 0;
    virtual bool IsTruthy () = 0;
    virtual Value* Call (VM* vm, std::vector<Value*> args); // Default just throws an exception.
    virtual bool EqualTo (Value* other) = 0;
	virtual Assignable* Dot (VM* vm, std::string key);
	virtual Assignable* Bracket (VM* vm, Value* arg);
};

#define ValueOverrides                                  \
    Value* Clone () override;                           \
    std::string ToString () override;                   \
    bool IsTruthy () override;                          \
    bool EqualTo (Value* other) override;

struct Assignable
{
	virtual ~Assignable ();
	virtual Value* Read (VM* vm) = 0;
    virtual Value* Write (VM* vm, Value* change_to);
};
struct NoAssignable : public Assignable
{
    Value* Read (VM* vm) override;
};
struct ReadOnlyAssignable : public Assignable
{
	Value* readable;
	ReadOnlyAssignable (Value* readable);
	~ReadOnlyAssignable () override;
	Value* Read (VM* vm) override;
};

struct NothingValue : public Value
{
    ValueOverrides
    NothingValue ();
};
struct IntValue : public Value
{
    ValueOverrides
    int value;
    IntValue (int value);
};
struct FloatValue : public Value
{
    ValueOverrides
    double value;
    FloatValue (double value);
};
struct StrValue : public Value
{
    ValueOverrides
    std::string str;
    StrValue (std::string str);
    Assignable* Bracket (VM* vm, Value* index) override;
    Assignable* Dot (VM* vm, std::string key) override;
};
struct StrAssignable : public Assignable
{
	StrValue* str_value;
	Value* index_value;
    StrAssignable (StrValue* str_value, Value* index_value);
    Value* Read (VM* vm) override;
    //Value* Write (VM* vm, Value* change_to) override;
};
struct CFuncValue : public Value
{
    ValueOverrides
    Value* Call (VM* vm, std::vector<Value*> args) override;
    Value* (*cfunc) (VM* vm, std::vector<Value*> args);
    CFuncValue (Value* (*cfunc) (VM* vm, std::vector<Value*> args));
};
struct Node;
struct VarFrame;
void ReserveVarFrame (VarFrame* frame);
void AbandonVarFrame (VarFrame* frame);
struct ScriptFunc
{
    Node* funcbody;
    VarFrame* parent_frame;
    int n_args;
    std::atomic_uint32_t refs {0};
    ScriptFunc (Node* funcbody, VarFrame* parent_frame, int n_args);
    ~ScriptFunc ();
};
struct ScriptFuncValue : public Value
{
    ValueOverrides
    Value* Call (VM* vm, std::vector<Value*> args) override;
    ScriptFunc* func_obj;
    ScriptFuncValue (ScriptFunc* func_obj);
    ~ScriptFuncValue () override;
};
struct MethodCallValue : public Value
{
    ValueOverrides
    Value* underlying_callable;
    Value* first_arg;
    MethodCallValue (Value* underlying_callable, Value* first_arg);
    ~MethodCallValue () override;
    Value* Call (VM* vm, std::vector<Value*> args) override;
};
struct ArrStorage
{
//private:
    std::mutex access_mutex;
    std::vector<Value*> values;
    friend class ArrValue;
    friend class ArrAssignable;
    friend class ArrBuilder; // Building array doesn't need synchronization for
                             // same reason deleting the items doesn't: in a
                             // situation where we are doing either, no other
                             // operation could be happening to this storage
                             // anyway.
public:
    std::atomic_uint32_t refs {0};

    ArrStorage* Clone ();
    ~ArrStorage ();
    int Length ();
};
struct ArrAssignable : public Assignable
{
	ArrStorage* storage;
	Value* index_value;
    ArrAssignable (ArrStorage* storage, Value* index_value);
    Value* Read (VM* vm) override;
    Value* Write (VM* vm, Value* change_to) override;
};
struct ArrValue : public Value
{
    ValueOverrides
    ArrStorage* storage;
    ArrValue (ArrStorage* storage);
    ~ArrValue () override;
    Assignable* Bracket (VM* vm, Value* arg) override;
    Assignable* Dot (VM* vm, std::string key) override;
};
struct ObjStorage
{
//private:
    std::unordered_map<std::string, Value*> values;
    std::mutex access_mutex;
    friend class ObjValue;
public:
    std::atomic_uint32_t refs {0};

    ObjStorage* Clone ();
    Value* Get (std::string key);
    void Set (std::string key, Value* val);
    ~ObjStorage ();
};
struct ObjAssignable : public Assignable
{
    ObjStorage* storage;
    std::string key;
    ObjAssignable (ObjStorage* storage, std::string key);
    Value* Read (VM* vm) override;
    Value* Write (VM* vm, Value* change_to) override;
};
struct ObjValue : public Value
{
    ValueOverrides
    ObjStorage* storage;
    ObjValue (ObjStorage* storage);
    ~ObjValue () override;
	Assignable* Dot (VM* vm, std::string key) override;
};
struct ThreadMeta
{
    std::atomic_uint32_t refs {0};
    /*  If the thread is running, it owns a reference.
        When it stops, it removes the reference, so if there are no variables,
        array entries, object values, etc. that point to the thread from the
        script, then the meta is just freed when its execution ends -- no need
        for any sort of explicit 'detach' mechanism.  */

    Value* retval;
    int unwind_type = UNWIND_NONE;
    /*  Waiting a thread that unwound due to an exception
        will throw the exception value in the thread that calls
        the wait function.  */

    //std::binary_semaphore done;
    semaphore done {0};
    /*  When the thread finishes, its return Value (so, (nothing) if there is
        no explicit return), is set and the semaphore is posted.
        Wait calls will wait for the semaphore, clone the return value, and
        post the semaphore themselves so that multiple waits just keep returning
        the value immediately.  */
};
struct ThreadValue : public Value
{
    ValueOverrides
    ThreadMeta* actual;
    ThreadValue (ThreadMeta* actual);
    ~ThreadValue () override;
	Assignable* Dot (VM* vm, std::string key) override;
};
void ThreadProc (ThreadMeta* thread, Value* func, std::vector<Value*> args);

struct ActualSem
{
    semaphore sem;
    ActualSem (int initial_value);
    void Post ();
    void Take ();
    bool TryTake ();
    std::atomic_uint32_t refs {0};
};
struct SemValue : public Value
{
    ValueOverrides
    ActualSem* actual;
    SemValue (ActualSem* actual);
    ~SemValue ();
	Assignable* Dot (VM* vm, std::string key) override;
};

struct Var
{
private:
    std::mutex modify_mutex;
    Value* value = new NothingValue;
public:
    void Set (Value* value);
    Value* Get ();
    ~Var ();
};

struct VarFrame
{
    std::vector<Var*> vars;
    VarFrame* parent_frame;
    std::atomic_uint32_t refs {0};
    VarFrame (VarFrame* parent_frame);
    ~VarFrame ();

private:
    std::mutex access_mutex;
    Var* ByID (int id);
    friend class VarHold;
};
void ReserveVarFrame (VarFrame* frame);
void AbandonVarFrame (VarFrame* frame);
struct VarHold
{
    std::unique_lock<std::mutex> frame_lock;
    Var* var;
    VarHold (VarFrame* frame, int id)
        : frame_lock(frame->access_mutex)
        , var(frame->ByID(id))
    {}
    VarHold (VarHold&& from)
        : frame_lock(std::move(from.frame_lock))
        , var(from.var)
    {}
    Var* operator-> ()
    {
        return var;
    }
};

struct VM /* Represents one thread of execution. */
{
private:
    // This value may be NULL even if we are unwinding, as a Value is only
    // produced sometimes, i.e. during UNWIND_RETURN or UNWIND_THROW.
    // for example if unwind_type is UNWIND_BREAK or UNWIND_CONTINUE.
    // It should certainly be NULL when we are not unwinding at all.
    Value* return_value = NULL;
    int unwind_type = UNWIND_NONE;
public:
    bool IsUnwinding ();
    void Return (Value* value);
    void Throw (Value* value);
    void Throw (std::string issue_text);
    void Break ();
    void Continue ();
    Value* CaptureReturn ();
    Value* CaptureThrown ();
    int CaptureBreakContinue ();

    VarFrame* current_frame = new VarFrame(NULL);
    void NewLocalFrame ();
    void PopLocalFrame ();

    ThreadMeta* thread_meta;
    VM (ThreadMeta* thread_meta = NULL);
    ~VM ();
};

struct VarRelLoc
{
    // variable's relative location
    int frames_upward = 0;
    int id_in_frame;
    VarHold GetVar (VM* vm);
};

struct Node
{
    virtual Value* Eval (VM* vm) = 0;
    virtual Value* Set (VM* vm, Value* set_to);
    virtual ~Node ();

    virtual Node* Clone () = 0;
    // Although cloning only happens when there is a compound assignment
    // (an lvalue is both the lvalue and part of the rvalue),
    // all types of Node (even non-lvalues) must implement this because
    // the lvalue might involve non-lvalue items.
    // Example: "my_array[i + 1] += 10;" -- the IntConst is not an lvalue
    // itself, but it is used while resolving the actual lvalue of the
    // array access. So, it must be cloned.
};
struct NothingLiteral : public Node
{
    Value* Eval (VM* vm) override;
    Node* Clone () override;
};
struct IntConst : public Node
{
    int value;
    Value* Eval (VM* vm) override;
    IntConst (int value);
    Node* Clone () override;
};
struct FloatConst : public Node
{
    double value;
    Value* Eval (VM* vm) override;
    FloatConst (double value);
    Node* Clone () override;
};
struct StrConst : public Node
{
    std::string str;
    Value* Eval (VM* vm) override;
    StrConst (std::string str);
    Node* Clone () override;
};
struct VarAccess : public Node
{
    VarRelLoc location;
    Value* Eval (VM* vm) override;
    Value* Set (VM* vm, Value* set_to) override;
    VarAccess (VarRelLoc location);
    Node* Clone () override;
};
struct IndexAccess : public Node
{
    Node* arr_node;
    Node* idx_expr;
    IndexAccess (Node* arr_node, Node* idx_expr);
    ~IndexAccess () override;
    Node* Clone () override;
    Value* Eval (VM* vm) override;
    Value* Set (VM* vm, Value* set_to) override;
};
struct DotAccess : public Node
{
    Node* leftside;
    std::string fieldname;
    DotAccess (Node* leftside, std::string fieldname);
    ~DotAccess () override;
    Node* Clone () override;
    Value* Eval (VM* vm) override;
    Value* Set (VM* vm, Value* set_to) override;
};

struct BinaryOp : public Node
{
    Node* left;
    Node* right;
    BinaryOp (Node* left, Node* right);
    Value* Eval (VM* vm) override;
    virtual Value* BinaryEval (Value* a, Value* b) = 0;
    ~BinaryOp () override;
};

struct Equal : public BinaryOp
{
    using BinaryOp::BinaryOp;
    Node* Clone () override;
    Value* BinaryEval (Value* a, Value* b) override;
};
struct NotEqual : public BinaryOp
{
    using BinaryOp::BinaryOp;
    Node* Clone () override;
    Value* BinaryEval (Value* a, Value* b) override;
};

struct BinaryMathOp : public BinaryOp
{
    using BinaryOp::BinaryOp;
    Value* BinaryEval (Value* a, Value* b) override;
    Value* IntsFloats (Value* a, Value* b);
    virtual Value* op_ints (int a, int b) = 0;
    virtual Value* op_floats (double a, double b) = 0;
};
#define DefOpSpecial(classname)                         \
    struct classname : public BinaryMathOp              \
    {                                                   \
        using BinaryMathOp::BinaryMathOp;               \
        Node* Clone () override;                        \
        Value* op_ints (int a, int b) override;         \
        Value* op_floats (double a, double b) override; \
        Value* BinaryEval (Value* a, Value* b) override;\
    };
#define DefOpPure(classname)                            \
    struct classname : public BinaryMathOp              \
    {                                                   \
        using BinaryMathOp::BinaryMathOp;               \
        Node* Clone () override;                        \
        Value* op_ints (int a, int b) override;         \
        Value* op_floats (double a, double b) override; \
    };
#define ImplOpFlex(classname,intexpr,floatexpr)                             \
    Node* classname::Clone ()                                               \
    {                                                                       \
        return new classname(left->Clone(), right->Clone());                \
    }                                                                       \
    Value* classname::op_ints (int a, int b) { return intexpr; }            \
    Value* classname::op_floats (double a, double b) { return floatexpr; }
#define ImplOpRigid(classname,op) \
    ImplOpFlex(classname, new IntValue(a op b), new FloatValue(a op b))
#define ImplOpCmp(classname, op) \
    ImplOpFlex(classname, new IntValue(a op b), new IntValue(a op b))

struct BinaryLogicOp : public BinaryOp
{
    using BinaryOp::BinaryOp;
    Value* BinaryEval (Value* a, Value* b) override;
    Value* Bools (Value* a, Value* b);
    virtual bool op_bools (bool a, bool b) = 0;
};
#define DefOpLogic(classname)                           \
    struct classname : public BinaryLogicOp             \
    {                                                   \
        using BinaryLogicOp::BinaryLogicOp;             \
        Node* Clone () override;                        \
        bool op_bools (bool a, bool b) override;        \
    };
#define ImplOpLogic(classname,expr)                                         \
    Node* classname::Clone ()                                               \
    {                                                                       \
        return new classname(left->Clone(), right->Clone());                \
    }                                                                       \
    bool classname::op_bools (bool a, bool b) { return expr; }

struct BinaryBitsOp : public BinaryOp
{
    using BinaryOp::BinaryOp;
    Value* BinaryEval (Value* a, Value* b) override;
    Value* Ints (Value* a, Value* b);
    virtual int op_ints (int a, int b) = 0;
};
#define DefOpBits(classname)                            \
    struct classname : public BinaryBitsOp              \
    {                                                   \
        using BinaryBitsOp::BinaryBitsOp;               \
        Node* Clone () override;                        \
        int op_ints (int a, int b) override;         \
    };
#define ImplOpBits(classname,op)                                            \
    Node* classname::Clone ()                                               \
    {                                                                       \
        return new classname(left->Clone(), right->Clone());                \
    }                                                                       \
    int classname::op_ints (int a, int b) { return a op b; }

DefOpSpecial(Add)
DefOpPure(Minus)
DefOpPure(Mul)
DefOpPure(Div)
DefOpPure(Modulus)
DefOpPure(Greater)
DefOpPure(GreaterEqual)
DefOpPure(Less)
DefOpPure(LessEqual)

DefOpLogic(LogicAnd)
DefOpLogic(LogicOr)
DefOpLogic(LogicXor)

DefOpBits(BitAnd)
DefOpBits(BitOr)
DefOpBits(BitXor)
DefOpBits(BitLeft)
DefOpBits(BitRight)

struct UnaryOp : public Node
{
    Node* inner;
    UnaryOp (Node* inner);
    ~UnaryOp () override;
    Value* Eval (VM* vm) override;
    virtual Value* UnaryEval (Value* value) = 0;
};
struct Negate : public UnaryOp
{
    using UnaryOp::UnaryOp;
    Value* UnaryEval (Value* value) override;
    Node* Clone () override;
};
struct LogicNot : public UnaryOp
{
    using UnaryOp::UnaryOp;
    Value* UnaryEval (Value* value) override;
    Node* Clone () override;
};
struct Assign : public Node
{
    Node* lvalue;
    Node* expr;
    Value* Eval (VM* vm) override;
    Assign (Node* lvalue, Node* expr);
    ~Assign () override;
    Node* Clone () override;
};
struct PostAssign : public Assign
{
    using Assign::Assign;
    Value* Eval (VM* vm) override;
    Node* Clone () override;
};
struct Scope;
struct Block : public Node
{
    std::vector<Node*> stmts;
    Block* Append (Node* stmt);
    ~Block () override;
    Value* Eval (VM* vm) override;
    Node* Clone () override;
};
struct FuncLiteralNode : public Node
{
    Node* funcbody;
    int n_args;
    FuncLiteralNode (Node* funcbody, int n_args);
    ~FuncLiteralNode () override;
    Value* Eval (VM* vm) override;
    Node* Clone () override;
};
struct FuncCall : public Node
{
    Node* func_node;
    std::vector<Node*> args;
    void AddArg (Node* arg);
    FuncCall (Node* func_node);
    ~FuncCall () override;
    Value* Eval (VM* vm) override;
    Node* Clone () override;
};
struct IfPair
{
    Node* cond;
    Node* block;
    IfPair (Node* cond, Node* block);
};
struct IfStmt : public Node
{
    std::vector<IfPair> pairs;
    IfStmt ();
    ~IfStmt () override;
    Value* Eval (VM* vm) override;
    Node* Clone () override;
};
struct WhileStmt : public Node
{
    Node* cond;
    Node* block;
    WhileStmt (Node* cond, Node* block);
    ~WhileStmt () override;
    Value* Eval (VM* vm) override;
    Node* Clone () override;
};
struct ForLoop : public Node
{
    Node* initial;
    Node* cond;
    Node* after;
    Node* block;
    ForLoop (Node* initial, Node* cond, Node* after, Node* block);
    ~ForLoop () override;
    Value* Eval (VM* vm) override;
    Node* Clone () override;
};
struct ArrBuilder : public Node
{
    std::vector<Node*> value_nodes;
    ~ArrBuilder () override;
    void Append (Node* node);
    Value* Eval (VM* vm) override;
    Node* Clone () override;
};
struct ObjBuilder : public Node
{
    std::vector<std::pair<Node*,Node*>> kv_nodes;
    ~ObjBuilder () override;
    void Append (Node* key, Node* value);
    Value* Eval (VM* vm) override;
    Node* Clone () override;
};
struct Return : public Node
{
    Node* expr;
    Return (Node* expr);
    ~Return () override;
    Value* Eval (VM* vm) override;
    Node* Clone () override;
};
struct Break : public Node
{
    Value* Eval (VM* vm) override;
    Node* Clone () override;
};
struct Continue : public Node
{
    Value* Eval (VM* vm) override;
    Node* Clone () override;
};
struct Throw : public Node
{
    Node* expr;
    Throw (Node* expr);
    ~Throw () override;
    Value* Eval (VM* vm) override;
    Node* Clone () override;
};
struct TryCatch : public Node
{
    Node* try_block;
    Node* catch_block;
    Node* catch_lvalue; // Allowed to be NULL (receiving the value is optional).
    TryCatch (Node* try_block, Node* catch_block, Node* catch_lvalue);
    ~TryCatch () override;
    Value* Eval (VM* vm) override;
    Node* Clone () override;
};
