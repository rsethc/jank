#pragma once

#define _GNU_SOURCE
#include "byte-source.h"
#include <string>

enum
{
    TokType_Invalid,

    TokType_EOF,

    TokType_Int,
    TokType_Float,
    TokType_Str,
    TokType_Ident,

    TokType_Semicolon,
    TokType_Colon,
    TokType_Comma,
    TokType_Dot,

    TokType_Add,
    TokType_Minus,
    TokType_Mul,
    TokType_Div,
    TokType_Modulus,

    TokType_Set,
    TokType_AddSet,
    TokType_MinusSet,
    TokType_MulSet,
    TokType_DivSet,
    TokType_ModulusSet,

    TokType_Incr,
    TokType_Decr,

    TokType_LogicNot,
    TokType_LogicAnd,
    TokType_LogicOr,
    TokType_LogicXor,

    TokType_BitAnd,
    TokType_BitOr,
    TokType_BitXor,
    TokType_BitLeft,
    TokType_BitRight,

    TokType_Equal,
    TokType_NotEqual,
    TokType_Greater,
    TokType_GreaterEqual,
    TokType_Less,
    TokType_LessEqual,

    TokType_Open,
    TokType_Close,
    TokType_Start,
    TokType_Finish,
    TokType_ArrStart,
    TokType_ArrFinish,

    TokType_If,
    TokType_Else,
    TokType_While,
    TokType_For,

    TokType_Func,
    TokType_Return,

    TokType_Break,
    TokType_Continue,

    TokType_Throw,
    TokType_Try,
    TokType_Catch,
};

struct Tok
{
    int type = TokType_Invalid;
    virtual ~Tok ();
};
struct SimpleTok : public Tok
{
    SimpleTok (int type);
};
struct IntTok : public Tok
{
    int value;
    IntTok (int value);
};
struct FloatTok : public Tok
{
    double value;
    FloatTok (double value);
};
struct StrTok : public Tok
{
    std::string str;
    StrTok (std::string str);
};
struct IdentTok : public Tok
{
    std::string str;
    IdentTok (std::string str);
};

struct Lex
{
    int nextch;
    ByteSource* byte_src;
    Lex (ByteSource* byte_src);
    void Advance ();
    ~Lex ();
    Tok* ReadNumeric ();
    Tok* ReadStr ();
    Tok* ReadIdent ();
    void ReadLineComment ();
    void ReadBlockComment ();
    Tok* ReadTok ();
};
