#pragma once

#include "lexer.h"
#include "vm.h"
#include <unordered_map>

struct Scope
{
    Scope* parent;
    Scope (Scope* parent, bool in_a_loop);
    ~Scope ();

    std::unordered_map<std::string, int> declared_vars;
    bool LocalLookup (char* name, VarRelLoc* rel_loc);
    bool RecursiveLookup (char* name, VarRelLoc* rel_loc);
    VarRelLoc LocalDeclare (char* name);
    VarRelLoc ByName (char* name);
    VarRelLoc ByNameLocal (char* name);

    bool in_a_loop = false;
};
struct Parse
{
    Lex* lex;
    Tok* next_tok_raw = NULL;
    Tok* NextTok ();
    Parse (Lex* lex);
    void Advance ();
    bool Expect (int type);
    ~Parse ();

    Scope* global_scope = new Scope(NULL, false);
    void NewScope (bool in_a_loop);
    void PopScope ();

    int func_impl_depth = 0;
    // Used for determining whether "return" statements are
    // allowed.
    Node* ReadFuncCall (Node* func_node);
    Node* ReadIndexAccess (Node* arr_node);
    Node* ReadArrayLiteral ();
    Node* ReadObjLiteral ();
    Node* Leaf ();
    Node* Expr8 (bool allow_func_call = true);
    Node* Expr7 ();
    Node* Expr6 ();
    Node* Expr5 ();
    Node* Expr4 ();
    Node* Expr3 ();
    Node* Expr2 ();
    Node* Expr1 ();
    Node* Expr0 ();
    Node* ReadIfWhile (bool is_while);
    Node* ReadFor ();
    Node* ReadBlock ();
    Node* ReadThrow ();
    Node* ReadTryCatch ();
    void DiscardSemicolons ();
    Node* ReadScriptFuncDefinition ();
    Node* ReadNamedScriptFunc ();
    Node* ReadAnonFuncLiteral ();
    Node* ReadReturn ();
    Node* ReadBreakContinue (bool is_continue);
    Node* ReadStmt ();
};
