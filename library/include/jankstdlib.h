#pragma once

#include "parser.h"
#include "vm.h"

struct NamedCFunc
{
    char* name;
    Value* (*cfunc) (VM* vm, std::vector<Value*> args);
    NamedCFunc (char* name, Value* (*cfunc) (VM* vm, std::vector<Value*> args));
};

void AddStandardLibrary (Parse* parse, VM* vm);
