#include <stdio.h>

class ByteSource
{
public:
	virtual int ReadByte () = 0;
	virtual ~ByteSource ();
};

class BorrowedFileByteSource : public ByteSource
{
protected:
	FILE* file;
public:
	BorrowedFileByteSource (FILE* file);
	int ReadByte () override;
};

class OwnedFileByteSource : public BorrowedFileByteSource
{
public:
	OwnedFileByteSource (FILE* file);
	OwnedFileByteSource (char* path);
	~OwnedFileByteSource () override;
};

class BorrowedMemoryByteSource : public ByteSource
{
	char* buffer;
	size_t remaining;
public:
	BorrowedMemoryByteSource (char* buffer, size_t length);
	int ReadByte () override;
};

class OwnedMemoryByteSource : public BorrowedMemoryByteSource
{
	char* buffer;
public:
	OwnedMemoryByteSource (char* buffer, size_t length);
	~OwnedMemoryByteSource () override;
};

/*
	Additional things you could make with custom ByteSource implementors:
	- An object that only reads from a certain region of a file, for example to
	  use when larger file formats contain scripts, to avoid extra copying.
	- Previous idea plus resetting the cursor in case something else on the same
	  thread may have read or written from somewhere else in same handle.
	- Previous idea plus locking a mutex to work safely on a shared file handle
	  with other threads.
	- Objects that provide bytes by decompressing from another ByteSource.
	- Objects that provide bytes by decrypting from another ByteSource.
	- Objects that provide bytes from network sources such as HTTP GET.
	- An object that can 'tee' something from a ByteSource into two outputs, for
	  use in implementing debugging or logging layers.
	- An object that eagerly reads from another ByteSource using a separate
	  thread to offload I/O tasks to a potentially different CPU core.
	- Previous idea but with a pool of one or more shared I/O threads instead of
	  one per ByteSource instance.
*/
