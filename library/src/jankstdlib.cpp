#include "jankstdlib.h"

#include <iostream>
#include <cstdio>
#include <mutex>
#include <cmath>

std::mutex print_mutex;
Value* print_impl (VM* vm, std::vector<Value*> args)
{
    std::lock_guard<decltype(print_mutex)> lock(print_mutex);
    bool first = true;
    for (Value* value : args)
    {
        if (first) first = false;
        else std::cout << ", ";

        std::cout << value->ToString();
    }
    std::cout << std::endl;
    return new NothingValue; // return nothing
}
Value* readline_impl (VM* vm, std::vector<Value*> args)
{
    if (args.size() > 1)
    {
        vm->Throw("expected 0 or 1 arguments to readline function");
        return new NothingValue;
    }

    if (args.size() == 1)
    {
        if (args[0]->type != VALUE_STR)
        {
            vm->Throw("argument to readline function must be a string");
            return new NothingValue;
        }
        StrValue* strval = (StrValue*)args[0];
        std::cout << strval->str << std::flush;
    }

    std::vector<char> line;
    while (true)
    {
        int ch = getchar();
        if (ch == '\n' || ch == EOF) break;
        line.push_back(ch);
    }
    line.push_back(0); // TO DO: eliminate null terminated strings
    return new StrValue(line.data());
}
Value* clone_impl (VM* vm, std::vector<Value*> args)
{
    if (args.size() != 1)
    {
        vm->Throw("expected 1 argument to clone function");
        return new NothingValue;
    }

    // The "Clone" C++ method is not doing what 'clone' in this context means,
    // i.e. for complex objects (arrays, objects, semaphores, whatever) it's
    // just creating copies of the Value objects.

    // So, for complex objects, this needs extra work.
    // TO DO: Handle cloning semaphores too by duplicating the actual semaphore
    //        object.
    // TO DO: Move this to a virtual method of Value so custom types can
    //        implement this behavior.

    switch (args[0]->type)
    {
        case VALUE_ARR:
        {
            ArrValue* val = (ArrValue*)args[0];
            return new ArrValue(val->storage->Clone());
        }

        case VALUE_OBJ:
        {
            ObjValue* val = (ObjValue*)args[0];
            return new ObjValue(val->storage->Clone());
        }

        default:
        return args[0]->Clone();
    }
}

Value* thread_start_impl (VM* vm, std::vector<Value*> args)
{
    if (args.size() < 1)
    {
        vm->Throw("expected at least 1 argument to thread_start function");
        return new NothingValue;
    }

    // Values from the original args vector are deleted
    // after this function returns, which is likely before the thread
    // will have finished running. So, we need to Clone everything that we
    // pass to the new thread, and it will then delete the clones when it's safe
    // to do so.

    Value* thread_func = args[0]->Clone();

    std::vector<Value*> thread_args;
    for (int i = 1; i < args.size(); i++)
    {
        thread_args.push_back(args[i]->Clone());
    }

    ThreadMeta* thread_meta = new ThreadMeta;
    thread_meta->refs++;
    Value* out = new ThreadValue(thread_meta);
    std::thread(ThreadProc,thread_meta,thread_func,thread_args).detach();

    return out;
}

Value* thread_wait_impl (VM* vm, std::vector<Value*> args)
{
    if (args.size() != 1)
    {
        vm->Throw("expected 1 argument to thread_wait function");
        return new NothingValue;
    }

    if (args[0]->type != VALUE_THREAD)
    {
        vm->Throw("non-thread type passed to thread_wait");
        return new NothingValue;
    }

    ThreadMeta* thread = ((ThreadValue*)args[0])->actual;

    // Wait until we know the thread exit has been signaled,
    // and re-post once we take the sem so that anything else
    // calling thread_wait on this same thread will also be free to unblock.
    thread->done.acquire();
    thread->done.release();

    // Cloning should be OK from multiple threads,
    // with the thread already done there's no reason these should change.
    if (thread->unwind_type == UNWIND_THROW)
    {
        vm->Throw(thread->retval->Clone());
        return new NothingValue;
    }
    else
    {
        return thread->retval->Clone();
    }
}

Value* sem_create_impl (VM* vm, std::vector<Value*> args)
{
    if (args.size() > 1)
    {
        vm->Throw("expected 0 or 1 args to sem_create");
        return new NothingValue;
    }

    int initial_value;
    if (args.size() == 1)
    {
        if (args[0]->type != VALUE_INT)
        {
            vm->Throw("argument to sem_create function must be int");
            return new NothingValue;
        }
        initial_value = ((IntValue*)args[0])->value;
    }
    else initial_value = 0;

    return new SemValue(new ActualSem(initial_value));
}

Value* sem_post_impl (VM* vm, std::vector<Value*> args)
{
    if (args.size() != 1)
    {
        vm->Throw("expected 1 argument to sem_post");
        return new NothingValue;
    }

    if (args[0]->type != VALUE_SEM)
    {
        vm->Throw("non-sem type passed to sem_post");
        return new NothingValue;
    }

    ActualSem* sem = ((SemValue*)args[0])->actual;
    sem->Post();
    return new NothingValue;
}

Value* sem_take_impl (VM* vm, std::vector<Value*> args)
{
    if (args.size() != 1)
    {
        vm->Throw("expected 1 argument to sem_take");
        return new NothingValue;
    }

    if (args[0]->type != VALUE_SEM)
    {
        vm->Throw("non-sem type passed to sem_take");
        return new NothingValue;
    }

    ActualSem* sem = ((SemValue*)args[0])->actual;
    sem->Take();
    return new NothingValue;
}

Value* sem_trytake_impl (VM* vm, std::vector<Value*> args)
{
    if (args.size() != 1)
    {
        vm->Throw("expected 1 argument to sem_trytake");
        return new NothingValue;
    }

    if (args[0]->type != VALUE_SEM)
    {
        vm->Throw("non-sem type passed to sem_trytake");
        return new NothingValue;
    }

    ActualSem* sem = ((SemValue*)args[0])->actual;
    bool acquired = sem->TryTake();
    return new IntValue(acquired);
}

NamedCFunc::NamedCFunc (char* name, Value* (*cfunc) (VM* vm, std::vector<Value*> args))
{
    this->name = name;
    this->cfunc = cfunc;
}

NamedCFunc stdlib [] =
{
    NamedCFunc("print",         print_impl          ),
    NamedCFunc("readline",      readline_impl       ),
    NamedCFunc("clone",         clone_impl          ),
    NamedCFunc("thread_start",  thread_start_impl   ),
    NamedCFunc("sem_create",    sem_create_impl     ),
};

// To do: reduce all of the redundant arguments-validation code.

Value* cos_impl (VM* vm, std::vector<Value*> args)
{
    if (args.size() != 1)
    {
        vm->Throw("expected 1 argument to cos");
        return new NothingValue;
    }

    double input;
    if (args[0]->type == VALUE_INT)
    {
        input = ((IntValue*)args[0])->value;
    }
    else if (args[0]->type == VALUE_FLOAT)
    {
        input = ((FloatValue*)args[0])->value;
    }
    else
    {
        vm->Throw("argument to cos must be int/float");
        return new NothingValue;
    }

    return new FloatValue(cos(input));
}

Value* sin_impl (VM* vm, std::vector<Value*> args)
{
    if (args.size() != 1)
    {
        vm->Throw("expected 1 argument to sin");
        return new NothingValue;
    }

    double input;
    if (args[0]->type == VALUE_INT)
    {
        input = ((IntValue*)args[0])->value;
    }
    else if (args[0]->type == VALUE_FLOAT)
    {
        input = ((FloatValue*)args[0])->value;
    }
    else
    {
        vm->Throw("argument to sin must be int/float");
        return new NothingValue;
    }

    return new FloatValue(sin(input));
}

Value* atan2_impl (VM* vm, std::vector<Value*> args)
{
    if (args.size() != 2)
    {
        vm->Throw("expected 2 argument to atan2");
        return new NothingValue;
    }

    double y_val;
    if (args[0]->type == VALUE_INT)
    {
        y_val = ((IntValue*)args[0])->value;
    }
    else if (args[0]->type == VALUE_FLOAT)
    {
        y_val = ((FloatValue*)args[0])->value;
    }
    else
    {
        vm->Throw("arguments to atan2 must be int/float");
        return new NothingValue;
    }
    double x_val;
    if (args[1]->type == VALUE_INT)
    {
        x_val = ((IntValue*)args[1])->value;
    }
    else if (args[1]->type == VALUE_FLOAT)
    {
        x_val = ((FloatValue*)args[1])->value;
    }
    else
    {
        vm->Throw("arguments to atan2 must be int/float");
        return new NothingValue;
    }

    return new FloatValue(atan2(y_val, x_val));
}

Value* pow_impl (VM* vm, std::vector<Value*> args)
{
    if (args.size() != 2)
    {
        vm->Throw("expected 2 argument to pow");
        return new NothingValue;
    }

    double base_val;
    if (args[0]->type == VALUE_INT)
    {
        base_val = ((IntValue*)args[0])->value;
    }
    else if (args[0]->type == VALUE_FLOAT)
    {
        base_val = ((FloatValue*)args[0])->value;
    }
    else
    {
        vm->Throw("arguments to pow must be int/float");
        return new NothingValue;
    }
    double exponent;
    if (args[1]->type == VALUE_INT)
    {
        exponent = ((IntValue*)args[1])->value;
    }
    else if (args[1]->type == VALUE_FLOAT)
    {
        exponent = ((FloatValue*)args[1])->value;
    }
    else
    {
        vm->Throw("arguments to pow must be int/float");
        return new NothingValue;
    }

    return new FloatValue(pow(base_val, exponent));
}

NamedCFunc mathlib [] =
{
    NamedCFunc("cos", cos_impl),
    NamedCFunc("sin", sin_impl),
    NamedCFunc("atan2", atan2_impl),
    NamedCFunc("pow", pow_impl),
};

void AddStandardLibrary (Parse* parse, VM* vm)
{
    for (int i = 0; i < sizeof(stdlib) / sizeof(*stdlib); i++)
    {
        NamedCFunc* func = stdlib + i;
        parse->global_scope->ByName(func->name)
            .GetVar(vm)->Set(new CFuncValue(func->cfunc));
    }

    // Add math library.
    ObjValue* mathobj = new ObjValue(new ObjStorage);
    parse->global_scope->ByName("math")
        .GetVar(vm)->Set(mathobj);
    for (int i = 0; i < sizeof(mathlib) / sizeof(*mathlib); i++)
    {
        NamedCFunc* func = mathlib + i;
        mathobj->storage->Set(func->name, new CFuncValue(func->cfunc));
    }
    mathobj->storage->Set("pi", new FloatValue(M_PI));
}
