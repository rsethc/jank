#include "vm.h"

#include <string.h>
#include <sstream>
#include <memory>
#include <thread>
#include <cmath>

Value::~Value () {}
Value* Value::Call (VM* vm, std::vector<Value*> args)
{
    vm->Throw("Value is not callable as a function.");
    return new NothingValue;
}
Assignable* Value::Dot (VM* vm, std::string key)
{
    return new NoAssignable;
}
Assignable* Value::Bracket (VM* vm, Value* index)
{
    return new NoAssignable;
}

NothingValue::NothingValue ()
{
    type = VALUE_NOTHING;
}
Value* NothingValue::Clone () { return new NothingValue; }
std::string NothingValue::ToString ()
{
    return "(nothing)";
}
bool NothingValue::IsTruthy ()
{
    return false;
}
bool NothingValue::EqualTo (Value* other)
{
    return type == other->type;
}

IntValue::IntValue (int value)
{
    type = VALUE_INT;
    this->value = value;
}
Value* IntValue::Clone () { return new IntValue(value); }
std::string IntValue::ToString ()
{
    std::ostringstream outstream;
    outstream << value;
    return outstream.str();
}
bool IntValue::IsTruthy ()
{
    return value != 0;
}
bool IntValue::EqualTo (Value* other)
{
    if (other->type == VALUE_INT)
        return value == ((IntValue*)other)->value;
    else if (other->type == VALUE_FLOAT)
        return value == ((FloatValue*)other)->value;
    else
        return false;
}

FloatValue::FloatValue (double value)
{
    type = VALUE_FLOAT;
    this->value = value;
}
Value* FloatValue::Clone () { return new FloatValue(value); }
std::string FloatValue::ToString ()
{
    std::ostringstream outstream;
    outstream << value;
    return outstream.str();
}
bool FloatValue::IsTruthy ()
{
    return value != 0;
}
bool FloatValue::EqualTo (Value* other)
{
    if (other->type == VALUE_INT)
        return value == ((IntValue*)other)->value;
    else if (other->type == VALUE_FLOAT)
        return value == ((FloatValue*)other)->value;
    else
        return false;
}

StrValue::StrValue (std::string str)
{
    type = VALUE_STR;
    this->str = str;
}
Value* StrValue::Clone () { return new StrValue(str); }
std::string StrValue::ToString ()
{
    return str;
}
bool StrValue::IsTruthy ()
{
    return str.length() != 0;
}
bool StrValue::EqualTo (Value* other)
{
    if (type != other->type) return false;
    return str == ((StrValue*)other)->str;
}
Assignable* StrValue::Dot (VM* vm, std::string key)
{
    if (key == "len")
        return new ReadOnlyAssignable(new IntValue(str.length()));
    else
        return new NoAssignable;
}
Assignable* StrValue::Bracket (VM* vm, Value* index)
{
    return new StrAssignable(this, index);
}

StrAssignable::StrAssignable (StrValue* str_value, Value* index_value)
    : str_value(str_value)
    , index_value(index_value)
{}
Value* StrAssignable::Read (VM* vm)
{
    if (index_value->type != VALUE_INT)
    {
        vm->Throw("int index required for string character access");
        return NULL;
    }

    int index = ((IntValue*)index_value)->value;
    if (index < 0)
    {
        vm->Throw("read index below zero");
        return NULL;
    }
    if (index >= str_value->str.length())
    {
        vm->Throw("read index out of bounds");
        return NULL;
    }

    return new IntValue(str_value->str[index]);
}
/*Value* StrAssignable::Write (VM* vm, Value* change_to)
{
    if (index_value->type != VALUE_INT)
    {
        vm->Throw("int index required for array access");
        return NULL;
    }

    if (change_to->type != VALUE_INT)
    {
        vm->Throw("character literal must be int type");
        return NULL;
    }

    int index = ((IntValue*)index_value)->value;
    if (index < 0)
    {
        vm->Throw("read index below zero");
        return NULL;
    }
    if (index > str_value->str.length())
    {
        vm->Throw("write index far beyond length");
        return NULL;
    }

    int character = ((IntValue*)change_to)->value;
    if (index == str_value->str.length())
    {
        str_value->str.push_back(character);
        return new NothingValue;
    }
    else
    {
        int prev_value = str_value->str[index];
        str_value->str[index] = character;
        return new IntValue(prev_value);
    }
}
*/

CFuncValue::CFuncValue (Value* (*cfunc) (VM* vm, std::vector<Value*> args))
{
    type = VALUE_CFUNC;
    this->cfunc = cfunc;
}
Value* CFuncValue::Clone () { return new CFuncValue(cfunc); }
std::string CFuncValue::ToString ()
{
    return "(native callable)";
}
bool CFuncValue::IsTruthy ()
{
    return true;
}
Value* CFuncValue::Call (VM* vm, std::vector<Value*> args)
{
    return cfunc(vm,args);
}
bool CFuncValue::EqualTo (Value* other)
{
    if (type != other->type) return false;
    return cfunc == ((CFuncValue*)other)->cfunc;
}

ScriptFunc::ScriptFunc (Node* funcbody, VarFrame* parent_frame, int n_args)
{
    this->funcbody = funcbody;
    this->parent_frame = parent_frame;
    ReserveVarFrame(parent_frame);
    this->n_args = n_args;
}
ScriptFunc::~ScriptFunc ()
{
    delete funcbody;
    AbandonVarFrame(parent_frame);
}

ScriptFuncValue::ScriptFuncValue (ScriptFunc* func_obj)
{
    type = VALUE_SCRIPTFUNC;
    this->func_obj = func_obj;
    func_obj->refs++;
}
Value* ScriptFuncValue::Clone () { return new ScriptFuncValue(func_obj); }
ScriptFuncValue::~ScriptFuncValue ()
{
    // The funcbody Node is owned by a FuncLiteralNode,
    // not owned by this value.

    if (!--func_obj->refs) delete func_obj;
}
std::string ScriptFuncValue::ToString ()
{
    return "(script callable)";
}
bool ScriptFuncValue::IsTruthy ()
{
    return true;
}
Value* ScriptFuncValue::Call (VM* vm, std::vector<Value*> args)
{
    VarFrame* prior_frame_ptr = vm->current_frame;
    vm->current_frame = new VarFrame(func_obj->parent_frame);
    ReserveVarFrame(vm->current_frame);

    for (int i = 0; i < args.size(); i++)
    {
        VarHold(vm->current_frame, i)
            ->Set(args[i]);
    }
    delete func_obj->funcbody->Eval(vm);

    AbandonVarFrame(vm->current_frame);
    vm->current_frame = prior_frame_ptr;

    // The way this already works will return a NothingValue if we are
    // currently unwinding due to an exception.
    Value* retval = vm->CaptureReturn();
    if (!retval) retval = new NothingValue;
    return retval;
}
bool ScriptFuncValue::EqualTo (Value* other)
{
    if (type != other->type) return false;
    return func_obj == ((ScriptFuncValue*)other)->func_obj;
}

MethodCallValue::MethodCallValue (Value* underlying_callable, Value* first_arg)
    : underlying_callable(underlying_callable)
    , first_arg(first_arg)
{
    type = VALUE_METHODCALL;
}
Value* MethodCallValue::Clone ()
{
    return new MethodCallValue(underlying_callable->Clone(),
                               first_arg->Clone());
}
MethodCallValue::~MethodCallValue ()
{
    delete underlying_callable;
    delete first_arg;
}
std::string MethodCallValue::ToString ()
{
    return "(method callable)";
}
bool MethodCallValue::IsTruthy ()
{
    return true;
}
Value* MethodCallValue::Call (VM* vm, std::vector<Value*> args)
{
    args.insert(args.begin(), first_arg->Clone());
    return underlying_callable->Call(vm, args);
}
bool MethodCallValue::EqualTo (Value* other)
{
    if (type != other->type) return false;
    MethodCallValue* other_method = (MethodCallValue*)other;
    return    underlying_callable->EqualTo(other_method->underlying_callable)
           && first_arg->EqualTo(other_method->first_arg);
}

ArrStorage* ArrStorage::Clone ()
{
    ArrStorage* out = new ArrStorage;
    std::lock_guard<decltype(access_mutex)> lock(access_mutex);
    for (Value* value : values)
    {
        out->values.push_back(value->Clone());
    }
    return out;
}
ArrStorage::~ArrStorage ()
{
    for (Value* value : values)
    {
        delete value;
    }
}
int ArrStorage::Length ()
{
    return values.size();
}

ObjStorage* ObjStorage::Clone ()
{
    ObjStorage* out = new ObjStorage;
    std::lock_guard<decltype(access_mutex)> lock(access_mutex);
    for (std::pair<std::string, Value*> entry : values)
    {
        out->Set(entry.first,entry.second->Clone());
    }
    return out;
}
Value* ObjStorage::Get (std::string key)
{
    std::lock_guard<decltype(access_mutex)> lock(access_mutex);
    try
    {
        return values.at(key)->Clone();
    }
    catch (std::out_of_range)
    {
        return NULL;
    }
}
void ObjStorage::Set (std::string key, Value* val)
{
    std::lock_guard<decltype(access_mutex)> lock(access_mutex);
    try
    {
        delete values.at(key);
    }
    catch (std::out_of_range) {}

    values[key] = val->Clone();
}
ObjStorage::~ObjStorage ()
{
    for (std::pair<std::string, Value*> entry : values)
    {
        delete entry.second;
    }
}

ArrValue::ArrValue (ArrStorage* storage)
{
    type = VALUE_ARR;
    this->storage = storage;
    storage->refs++;
}
Value* ArrValue::Clone ()
{
    return new ArrValue(storage);
}
ArrValue::~ArrValue ()
{
    if (!--storage->refs) delete storage;
}
std::string ArrValue::ToString ()
{
    std::ostringstream outstream;
    outstream << '[';
    bool first = true;

    {
        std::lock_guard<decltype(storage->access_mutex)> lock(storage->access_mutex);

        for (Value* arritem : storage->values)
        {
            if (first) first = false;
            else outstream << ',';
            outstream << ' ';
            outstream << arritem->ToString();
        }
    }

    outstream << " ]";
    return outstream.str();
}
bool ArrValue::IsTruthy ()
{
    return true;
}
bool ArrValue::EqualTo (Value* other)
{
    if (type != other->type) return false;
    return storage == ((ArrValue*)other)->storage;
}
Assignable* ArrValue::Bracket (VM* vm, Value* index)
{
    return new ArrAssignable(storage, index);
}
Assignable* ArrValue::Dot (VM* vm, std::string key)
{
    if (key == "len")
        return new ReadOnlyAssignable(new IntValue(storage->values.size()));
    else
        return new NoAssignable;
}

ArrAssignable::ArrAssignable (ArrStorage* storage, Value* index_value)
    : storage(storage)
    , index_value(index_value)
{}
Value* ArrAssignable::Read (VM* vm)
{
    if (index_value->type != VALUE_INT)
    {
        vm->Throw("int index required for array access");
        return NULL;
    }

    int index = ((IntValue*)index_value)->value;
    if (index < 0)
    {
        vm->Throw("read index below zero");
        return NULL;
    }

    std::lock_guard<decltype(storage->access_mutex)> lock(storage->access_mutex);

    if (index >= storage->values.size())
    {
        vm->Throw("read index out of bounds");
        return NULL;
    }

    return storage->values[index]->Clone();
}
Value* ArrAssignable::Write (VM* vm, Value* change_to)
{
    if (index_value->type != VALUE_INT)
    {
        vm->Throw("int index required for array access");
        return NULL;
    }

    int index = ((IntValue*)index_value)->value;
    if (index < 0)
    {
        vm->Throw("read index below zero");
        return NULL;
    }

    std::lock_guard<decltype(storage->access_mutex)> lock(storage->access_mutex);

    if (index > storage->values.size())
    {
        vm->Throw("write index far beyond length");
        return NULL;
    }

    if (index == storage->values.size())
    {
        storage->values.push_back(change_to->Clone());
        return new NothingValue;
    }
    else
    {
        Value* prev_value = storage->values[index];
        storage->values[index] = change_to->Clone();
        return prev_value;
    }
}

ObjValue::ObjValue (ObjStorage* storage)
{
    type = VALUE_OBJ;
    this->storage = storage;
    storage->refs++;
}
Value* ObjValue::Clone ()
{
    return new ObjValue(storage);
}
ObjValue::~ObjValue ()
{
    if (!--storage->refs) delete storage;
}
std::string ObjValue::ToString ()
{
    std::ostringstream outstream;
    outstream << "{ ";
    bool first = true;

    {
        std::lock_guard<decltype(storage->access_mutex)> lock(storage->access_mutex);

        for (std::pair<std::string,Value*> entry : storage->values)
        {
            if (first) first = false;
            else outstream << ", ";
            outstream << '"' << entry.first << "\": ";
            outstream << entry.second->ToString();
        }
    }

    outstream << " }";
    return outstream.str();
}
bool ObjValue::IsTruthy ()
{
    return true;
}
bool ObjValue::EqualTo (Value* other)
{
    if (type != other->type) return false;
    return storage == ((ObjValue*)other)->storage;
}
Assignable* ObjValue::Dot (VM* vm, std::string key)
{
    return new ObjAssignable(storage,key);
}

ObjAssignable::ObjAssignable (ObjStorage* storage, std::string key)
    : storage(storage)
    , key(key)
{}
Value* ObjAssignable::Read (VM* vm)
{
    Value* out = storage->Get(key);
    if (!out)
    {
        vm->Throw("can't read object field that doesn't exist yet");
        return NULL;
    }
    return out;
}
Value* ObjAssignable::Write (VM* vm, Value* change_to)
{
    Value* prev = storage->Get(key);
    if (!prev) prev = new NothingValue;
    storage->Set(key, change_to);
    return prev;
}

ThreadValue::ThreadValue (ThreadMeta* actual)
{
    type = VALUE_THREAD;
    this->actual = actual;
    actual->refs++;
}
ThreadValue::~ThreadValue ()
{
    if (!--actual->refs) delete actual;
}
Value* ThreadValue::Clone ()
{
    return new ThreadValue(actual);
}
bool ThreadValue::IsTruthy ()
{
    return true;
}
std::string ThreadValue::ToString ()
{
    bool running = actual->unwind_type != UNWIND_NONE;
    std::string out = "(thread <";
    out += running ?           "done>)"
                             : "running>)";
    return out;
}
bool ThreadValue::EqualTo (Value* other)
{
    if (type != other->type) return false;
    return actual == ((ThreadValue*)other)->actual;
}
Value* thread_wait_impl (VM* vm, std::vector<Value*> args);
Assignable* ThreadValue::Dot (VM* vm, std::string key)
{
    if (key == "wait")
        return new ReadOnlyAssignable(
            new MethodCallValue(
                new CFuncValue(thread_wait_impl),
                Clone()
            )
        );
    else
        return new NoAssignable;
}

void ThreadProc (ThreadMeta* thread, Value* func, std::vector<Value*> args)
{
    VM* vm = new VM(thread);

    Value* retval = func->Call(vm,args);
    /* detect return (or thrown exception) and set it in the meta object */

    // It shouldn't be possible that we're unwinding
    // for any reason other than an exception, here.
    Value* thrown = vm->CaptureThrown();

    delete vm;

    if (thrown)
    {
        delete retval;
        thread->retval = thrown;
        thread->unwind_type = UNWIND_THROW;
    }
    else
    {
        thread->retval = retval;
        thread->unwind_type = UNWIND_RETURN;
    }
    thread->done.release();

    // Thread owns the function and args it was passed,
    // delete them to avoid memory leak.
    for (Value* arg : args)
    {
        delete arg;
    }
    delete func;

    if (!--thread->refs) delete thread;
}

ActualSem::ActualSem (int initial_value)
    : sem(initial_value)
{}
void ActualSem::Post ()
{
    sem.release();
}
void ActualSem::Take ()
{
    sem.acquire();
}
bool ActualSem::TryTake ()
{
    return sem.try_acquire();
}

SemValue::SemValue (ActualSem* actual)
{
    type = VALUE_SEM;
    this->actual = actual;
    actual->refs++;
}
SemValue::~SemValue ()
{
    if (!--actual->refs) delete actual;
}
Value* SemValue::Clone ()
{
    return new SemValue(actual);
}
bool SemValue::IsTruthy ()
{
    return true;
}
std::string SemValue::ToString ()
{
    return "(semaphore)";
}
bool SemValue::EqualTo (Value* other)
{
    if (type != other->type) return false;
    return actual == ((SemValue*)other)->actual;
}
Value* sem_post_impl (VM* vm, std::vector<Value*> args);
Value* sem_take_impl (VM* vm, std::vector<Value*> args);
Value* sem_trytake_impl (VM* vm, std::vector<Value*> args);
Assignable* SemValue::Dot (VM* vm, std::string key)
{
    if (key == "post")
        return new ReadOnlyAssignable(
            new MethodCallValue(
                new CFuncValue(sem_post_impl),
                Clone()
            )
        );
    else if (key == "take")
        return new ReadOnlyAssignable(
            new MethodCallValue(
                new CFuncValue(sem_take_impl),
                Clone()
            )
        );
    else if (key == "trytake")
        return new ReadOnlyAssignable(
            new MethodCallValue(
                new CFuncValue(sem_trytake_impl),
                Clone()
            )
        );
    else
        return new NoAssignable;
}

void Var::Set (Value* value)
{
    std::lock_guard<decltype(modify_mutex)> lock(modify_mutex);
    delete this->value;
    this->value = value->Clone();
}
Value* Var::Get ()
{
    std::lock_guard<decltype(modify_mutex)> lock(modify_mutex);
    return value;
}
Var::~Var ()
{
    delete value;
}

Assignable::~Assignable () {}
Value* Assignable::Write (VM* vm, Value* change_to)
{
	vm->Throw("can't write to field");
	return NULL;
}

Value* NoAssignable::Read (VM* vm)
{
    vm->Throw("can't read from field");
    return NULL;
}

ReadOnlyAssignable::ReadOnlyAssignable (Value* readable)
{
	this->readable = readable;
}
ReadOnlyAssignable::~ReadOnlyAssignable ()
{
    if (readable) delete readable;
}
Value* ReadOnlyAssignable::Read (VM* vm)
{
	Value* out = readable;
    readable = NULL;
    return out;
}

VarFrame::VarFrame (VarFrame* parent_frame)
{
    this->parent_frame = parent_frame;
}
VarFrame::~VarFrame ()
{
    for (Var* var : vars)
    {
        delete var;
    }
}
Var* VarFrame::ByID (int id)
{
    while (vars.size() <= id)
        vars.push_back(new Var);
    return vars[id];
}
void ReserveVarFrame (VarFrame* frame)
{
    while (frame)
    {
        frame->refs++;
        frame = frame->parent_frame;
    }
}
void AbandonVarFrame (VarFrame* frame)
{
    while (frame)
    {
        VarFrame* parent = frame->parent_frame;
        if (!--frame->refs) delete frame;
        frame = parent;
    }
}

VM::VM (ThreadMeta* thread_meta)
{
    this->thread_meta = thread_meta;
    ReserveVarFrame(current_frame);
}
VM::~VM ()
{
    AbandonVarFrame(current_frame);
    if (return_value) delete return_value;
}
void VM::Return (Value* value)
{
    // If already unwinding, discard anything further.
    // I suspect this could happen if a Return statement is what's executing
    // some callee that throws an exception.
    if (IsUnwinding())
    {
        delete value;
        return;
    }

    return_value = value;
    unwind_type = UNWIND_RETURN;
}
void VM::Throw (Value* value)
{
    // If already unwinding, discard anything further.
    // I suspect this could happen if a Return statement is what's executing
    // some callee that throws an exception.
    if (IsUnwinding())
    {
        delete value;
        return;
    }

    return_value = value;
    unwind_type = UNWIND_THROW;
}
void VM::Throw (std::string issue_text)
{
    // To do: make an ObjValue instead of just a StrValue,
    // with the issue_text as just one field, and also some information
    // detailing the call stack, line number and column number, etc
    Throw(new StrValue(issue_text));
}
void VM::Break ()
{
    if (unwind_type == UNWIND_NONE) unwind_type = UNWIND_BREAK;
}
void VM::Continue ()
{
    if (unwind_type == UNWIND_NONE) unwind_type = UNWIND_CONTINUE;
}
bool VM::IsUnwinding ()
{
    return unwind_type != UNWIND_NONE;
}
Value* VM::CaptureReturn ()
{
    if (unwind_type != UNWIND_RETURN) return NULL;

    Value* out = return_value;
    return_value = NULL;
    unwind_type = UNWIND_NONE;
    return out;
}
Value* VM::CaptureThrown ()
{
    if (unwind_type != UNWIND_THROW) return NULL;

    Value* out = return_value;
    return_value = NULL;
    unwind_type = UNWIND_NONE;
    return out;
}
int VM::CaptureBreakContinue ()
{
    switch (unwind_type)
    {
        case UNWIND_BREAK:
        case UNWIND_CONTINUE:
        int was = unwind_type;
        unwind_type = UNWIND_NONE;
        return was;

        return UNWIND_NONE;
    }
}
void VM::NewLocalFrame ()
{
    current_frame = new VarFrame(current_frame);
    ReserveVarFrame(current_frame);
}
void VM::PopLocalFrame ()
{
    VarFrame* exited = current_frame;
    current_frame = exited->parent_frame;
    AbandonVarFrame(exited);
}
/*VarFrame* VM::NewFuncFrame (VarFrame* parent)
{
    VarFrame* previous = current_frame;
    current_frame = new VarFrame(parent);
    ReserveVarFrame(current_frame);
    return previous;
}
void VM::PopFuncFrame (VarFrame* previous)
{
    AbandonVarFrame(current_frame);
    current_frame = previous;
}*/

Value* Node::Set (VM* vm, Value* set_to)
{
    // When overridden, the return value should be
    // the previous value prior to setting.
    // set_to is owned by the original caller, must not be
    // freed on error and must be cloned when setting.
    return NULL;
}
Node::~Node () {}

Value* NothingLiteral::Eval (VM* vm)
{
    return new NothingValue;
}
Node* NothingLiteral::Clone ()
{
    return new NothingLiteral;
}

Value* IntConst::Eval (VM* vm)
{
    return new IntValue(value);
}
IntConst::IntConst (int value)
{
    this->value = value;
}
Node* IntConst::Clone () { return new IntConst(value); }

Value* FloatConst::Eval (VM* vm)
{
    return new FloatValue(value);
}
FloatConst::FloatConst (double value)
{
    this->value = value;
}
Node* FloatConst::Clone () { return new FloatConst(value); }

Value* StrConst::Eval (VM* vm)
{
    return new StrValue(str);
}
StrConst::StrConst (std::string str)
{
    this->str = str;
}
Node* StrConst::Clone () { return new StrConst(str); }

Value* VarAccess::Eval (VM* vm)
{
    VarHold var = location.GetVar(vm);
    return var->Get()->Clone();
}
Value* VarAccess::Set (VM* vm, Value* set_to)
{
    VarHold var = location.GetVar(vm);
    Value* out = var->Get()->Clone();
    var->Set(set_to);
    return out;
}
VarAccess::VarAccess (VarRelLoc location)
{
    this->location = location;
}
Node* VarAccess::Clone () { return new VarAccess(location); }

IndexAccess::IndexAccess (Node* arr_node, Node* idx_expr)
{
    this->arr_node = arr_node;
    this->idx_expr = idx_expr;
}
IndexAccess::~IndexAccess ()
{
    delete arr_node;
    delete idx_expr;
}
Node* IndexAccess::Clone () { return new IndexAccess(arr_node->Clone(), idx_expr->Clone()); }
Value* IndexAccess::Eval (VM* vm)
{
    std::unique_ptr<Value> bracket_host (arr_node->Eval(vm));
    if (vm->IsUnwinding()) return new NothingValue;

    std::unique_ptr<Value> idx (idx_expr->Eval(vm));
    if (vm->IsUnwinding()) return new NothingValue;

    std::unique_ptr<Assignable> assignable (bracket_host->Bracket(vm, idx.get()));

    // Assignable::Write is supposed to give NULL if it throws.
    // Eval is not allowed to return NULL so create a Nothing for safety.
    Value* out = assignable->Read(vm);
    if (!out) out = new NothingValue;
    return out;
}
Value* IndexAccess::Set (VM* vm, Value* set_to)
{
    std::unique_ptr<Value> bracket_host (arr_node->Eval(vm));
    if (vm->IsUnwinding()) return new NothingValue;

    std::unique_ptr<Value> idx (idx_expr->Eval(vm));
    if (vm->IsUnwinding()) return new NothingValue;

    std::unique_ptr<Assignable> assignable (bracket_host->Bracket(vm, idx.get()));

    // Assignable::Write is supposed to give NULL if it throws.
    // Set is allowed to return NULL.
    return assignable->Write(vm, set_to);
}

DotAccess::DotAccess (Node* leftside, std::string fieldname)
{
    this->leftside = leftside;
    this->fieldname = fieldname;
}
DotAccess::~DotAccess ()
{
    delete leftside;
}
Node* DotAccess::Clone () { return new DotAccess(leftside->Clone(), fieldname); }
Value* DotAccess::Eval (VM* vm)
{
    std::unique_ptr<Value> dot_host (leftside->Eval(vm));
    if (vm->IsUnwinding()) return new NothingValue;

    std::unique_ptr<Assignable> assignable (dot_host->Dot(vm, fieldname));

    // Assignable::Write is supposed to give NULL if it throws.
    // Eval is not allowed to return NULL so create a Nothing for safety.
    Value* out = assignable->Read(vm);
    if (!out) out = new NothingValue;
    return out;
}
Value* DotAccess::Set (VM* vm, Value* set_to)
{
    std::unique_ptr<Value> dot_host (leftside->Eval(vm));
    if (vm->IsUnwinding()) return NULL;

    std::unique_ptr<Assignable> assignable (dot_host->Dot(vm, fieldname));

    // Assignable::Write is supposed to give NULL if it throws.
    // Set is allowed to return NULL.
    return assignable->Write(vm, set_to);
}

BinaryOp::BinaryOp (Node* left, Node* right)
{
    this->left = left;
    this->right = right;
}
Value* BinaryOp::Eval (VM* vm)
{
    std::unique_ptr<Value> a(left->Eval(vm));
    if (vm->IsUnwinding()) return new NothingValue;

    std::unique_ptr<Value> b(right->Eval(vm));
    if (vm->IsUnwinding()) return new NothingValue;

    return BinaryEval(a.get(),b.get());
}
BinaryOp::~BinaryOp ()
{
    delete left;
    delete right;
}

Value* BinaryMathOp::BinaryEval (Value* a, Value* b)
{
    return IntsFloats(a,b);
}
Value* BinaryMathOp::IntsFloats (Value* a, Value* b)
{
    if (a->type == VALUE_FLOAT || b->type == VALUE_FLOAT)
    {
        double a_value, b_value;

        if (a->type == VALUE_INT) a_value = ((IntValue*)a)->value;
        else if (a->type == VALUE_FLOAT) a_value = ((FloatValue*)a)->value;
        else return new NothingValue;

        if (b->type == VALUE_INT) b_value = ((IntValue*)b)->value;
        else if (b->type == VALUE_FLOAT) b_value = ((FloatValue*)b)->value;
        else return new NothingValue;

        return op_floats(a_value,b_value);
    }
    else if (a->type == VALUE_INT && b->type == VALUE_INT)
    {
        int a_value = ((IntValue*)a)->value,
            b_value = ((IntValue*)b)->value;
        return op_ints(a_value,b_value);
    }
    else return new NothingValue;
}

Value* BinaryLogicOp::BinaryEval (Value* a, Value* b)
{
    return Bools(a,b);
}
Value* BinaryLogicOp::Bools (Value* a, Value* b)
{
    bool truthy_a = a->IsTruthy(),
         truthy_b = b->IsTruthy();
    return new IntValue(op_bools(truthy_a,truthy_b));
}

Value* BinaryBitsOp::BinaryEval (Value* a, Value* b)
{
    return Ints(a,b);
}
Value* BinaryBitsOp::Ints (Value* a, Value* b)
{
    if (a->type == VALUE_INT && b->type == VALUE_INT)
    {
        int a_value = ((IntValue*)a)->value,
            b_value = ((IntValue*)b)->value;
        return new IntValue(op_ints(a_value,b_value));
    }
    else return new NothingValue;
}

ImplOpRigid(Add, +)
Value* Add::BinaryEval (Value* a, Value* b)
{
    // Overriding this in order to implement string addition
    // in addition to the numeric operation that the inherited BinaryEval
    // would handle.
    if (a->type == VALUE_STR || b->type == VALUE_STR)
        return new StrValue(a->ToString() + b->ToString());
    else
        return IntsFloats(a,b);
}
ImplOpRigid(Minus, -)
ImplOpRigid(Mul, *)
ImplOpRigid(Div, /)
ImplOpFlex(Modulus,
           new IntValue(a % b),
           new FloatValue(fmod(a,b)))
Value* Equal::BinaryEval (Value* a, Value* b)
{
    return new IntValue(a->EqualTo(b));
}
Node* Equal::Clone ()
{
    return new Equal(left->Clone(), right->Clone());
}
Value* NotEqual::BinaryEval (Value* a, Value* b)
{
    return new IntValue(!a->EqualTo(b));
}
Node* NotEqual::Clone ()
{
    return new NotEqual(left->Clone(), right->Clone());
}
ImplOpCmp(Greater, >)
ImplOpCmp(GreaterEqual, >=)
ImplOpCmp(Less, <)
ImplOpCmp(LessEqual, <=)
ImplOpLogic(LogicAnd, a && b)
ImplOpLogic(LogicOr, a || b)
ImplOpLogic(LogicXor, !a != !b)
ImplOpBits(BitAnd, &)
ImplOpBits(BitOr, |)
ImplOpBits(BitXor, ^)
ImplOpBits(BitLeft, <<)
ImplOpBits(BitRight, >>)

UnaryOp::UnaryOp (Node* inner)
{
    this->inner = inner;
}
UnaryOp::~UnaryOp ()
{
    delete inner;
}
Value* UnaryOp::Eval (VM* vm)
{
    std::unique_ptr<Value> value (inner->Eval(vm));
    if (vm->IsUnwinding()) return new NothingValue;
    return UnaryEval(value.get());
}

Value* Negate::UnaryEval (Value* value)
{
    if (value->type == VALUE_INT)
    {
        return new IntValue(-((IntValue*)value)->value);
    }
    else if (value->type == VALUE_FLOAT)
    {
        return new FloatValue(-((FloatValue*)value)->value);
    }
    else return new NothingValue;
}
Node* Negate::Clone () { return new Negate(inner->Clone()); }

Value* LogicNot::UnaryEval (Value* value)
{
    if (value->type == VALUE_INT)
    {
        return new IntValue(!((IntValue*)value)->value);
    }
    else return new NothingValue;
}
Node* LogicNot::Clone () { return new LogicNot(inner->Clone()); }

Value* Assign::Eval (VM* vm)
{
    Value* now = expr->Eval(vm);
    Value* prev;
    if (vm->IsUnwinding()) goto CLEANUP_NOW;

    prev = lvalue->Set(vm,now);
    if (vm->IsUnwinding()) goto CLEANUP_PREV;
    if (!prev)
    {
        vm->Throw("setting failed (assign)");
        goto CLEANUP_NOW;
    }

    delete prev;
    return now;

    CLEANUP_PREV:
    if (prev) delete prev;
    CLEANUP_NOW:
    delete now;
    return new NothingValue;
}
Assign::Assign (Node* lvalue, Node* expr)
{
    this->lvalue = lvalue;
    this->expr = expr;
}
Assign::~Assign ()
{
    delete lvalue;
    delete expr;
}
Node* Assign::Clone () { return new Assign(lvalue->Clone(),expr->Clone()); }

Value* PostAssign::Eval (VM* vm)
{
    Value* now = expr->Eval(vm);
    if (vm->IsUnwinding())
    {
        delete now;
        return new NothingValue;
    }

    Value* prev = lvalue->Set(vm,now);
    delete now;
    if (vm->IsUnwinding())
    {
        if (prev) delete prev;
        return new NothingValue;
    }
    if (!prev)
    {
        vm->Throw("setting failed (post-assign)");
        return new NothingValue;
    }
    return prev;
}
Node* PostAssign::Clone () { return new PostAssign(lvalue->Clone(),expr->Clone()); }

Block* Block::Append (Node* stmt)
{
    stmts.push_back(stmt);
    return this;
}
Block::~Block ()
{
    for (Node* stmt : stmts)
    {
        delete stmt;
    }
}
Value* Block::Eval (VM* vm)
{
    vm->NewLocalFrame();

    for (Node* stmt : stmts)
    {
        delete stmt->Eval(vm);
        if (vm->IsUnwinding()) break;
    }

    vm->PopLocalFrame();

    return new NothingValue;
}
Node* Block::Clone ()
{
    Block* out = new Block;
    for (Node* stmt : stmts)
    {
        out->Append(stmt->Clone());
    }
    return out;
}

FuncLiteralNode::FuncLiteralNode (Node* funcbody, int n_args)
{
    this->funcbody = funcbody;
    this->n_args = n_args;
}
FuncLiteralNode::~FuncLiteralNode ()
{
    delete funcbody;
}
Value* FuncLiteralNode::Eval (VM* vm)
{
    ScriptFunc* func_obj = new ScriptFunc(funcbody->Clone(), vm->current_frame, n_args);
    return new ScriptFuncValue(func_obj);
}
Node* FuncLiteralNode::Clone () { return new FuncLiteralNode(funcbody->Clone(), n_args); }

void FuncCall::AddArg (Node* arg)
{
    args.push_back(arg);
}
FuncCall::FuncCall (Node* func_node)
{
    this->func_node = func_node;
}
FuncCall::~FuncCall ()
{
    for (Node* arg : args)
    {
        delete arg;
    }
}
Value* FuncCall::Eval (VM* vm)
{
    std::unique_ptr<Value> func_value(func_node->Eval(vm));
    if (vm->IsUnwinding()) return new NothingValue;

    Value* retval;

    std::vector<Value*> arg_values;
    for (Node* arg_node : args)
    {
        Value* arg_value = arg_node->Eval(vm);
        if (vm->IsUnwinding())
        {
            delete arg_value;
            retval = new NothingValue;
            goto ARGS_CLEANUP;
        }
        arg_values.push_back(arg_value);
    }

    retval = func_value->Call(vm, arg_values);
    if (vm->IsUnwinding())
    {
        delete retval;
        retval = new NothingValue;
        goto ARGS_CLEANUP;
    }

    ARGS_CLEANUP:
    for (Value* arg_value : arg_values)
    {
        delete arg_value;
    }
    return retval;
}
Node* FuncCall::Clone ()
{
    FuncCall* out = new FuncCall(func_node);
    for (Node* arg_node : args)
    {
        out->AddArg(arg_node->Clone());
    }
    return out;
}

bool EvalCond (Node* cond, VM* vm)
{
    std::unique_ptr<Value> result (cond->Eval(vm));
    return result->IsTruthy();
}

IfPair::IfPair (Node* cond, Node* block)
{
    this->cond = cond;
    this->block = block;
}

IfStmt::IfStmt () {}
IfStmt::~IfStmt ()
{
    for (IfPair pair : pairs)
    {
        delete pair.cond;
        delete pair.block;
    }
}
Value* IfStmt::Eval (VM* vm)
{
    for (IfPair pair : pairs)
    {
        vm->NewLocalFrame();
        bool cond_true = EvalCond(pair.cond,vm);
        if (vm->IsUnwinding())
        {
            vm->PopLocalFrame();
            break;
        }
        if (!cond_true)
        {
            vm->PopLocalFrame();
            continue;
        }

        delete pair.block->Eval(vm);
        vm->PopLocalFrame();
        //if (vm->IsUnwinding()) break;
        break; // We break even if the VM is not unwinding.
               // Don't do any of the else-if's or the else since the condition
               // for this pair was already true.
    }
    return new NothingValue;
}
Node* IfStmt::Clone ()
{
    IfStmt* cloned = new IfStmt;
    for (IfPair pair : pairs)
    {
        cloned->pairs.push_back(IfPair(pair.cond->Clone(),
                                       pair.block->Clone()));
    }
    return cloned;
}

WhileStmt::WhileStmt (Node* cond, Node* block)
{
    this->cond = cond;
    this->block = block;
}
WhileStmt::~WhileStmt ()
{
    delete cond;
    delete block;
}
Value* WhileStmt::Eval (VM* vm)
{
    vm->NewLocalFrame();

    while (true)
    {
        bool cond_true = EvalCond(cond,vm);
        if (!cond_true || vm->IsUnwinding()) break;

        delete block->Eval(vm);
        int loop_signal = vm->CaptureBreakContinue();
        if (loop_signal == UNWIND_BREAK) break;
        // For UNWIND_CONTINUE we just continue, the inner Node has already
        // unwound by this point.
        // Still check for other kinds of unwinding (i.e. return, throw):
        if (vm->IsUnwinding()) break;
    }

    vm->PopLocalFrame();
    return new NothingValue;
}
Node* WhileStmt::Clone () { return new WhileStmt(cond->Clone(), block->Clone()); }

ForLoop::ForLoop (Node* initial, Node* cond, Node* after, Node* block)
{
    this->initial = initial;
    this->cond = cond;
    this->after = after;
    this->block = block;
}
ForLoop::~ForLoop ()
{
    delete initial;
    delete cond;
    delete after;
    delete block;
}
Value* ForLoop::Eval (VM* vm)
{
    vm->NewLocalFrame();

    delete initial->Eval(vm);
    if (vm->IsUnwinding())
    {
        vm->PopLocalFrame();
        return new NothingValue;
    }

    while (true)
    {
        bool cond_true = EvalCond(cond,vm);
        if (!cond_true || vm->IsUnwinding()) break;

        delete block->Eval(vm);
        int loop_signal = vm->CaptureBreakContinue();
        if (loop_signal == UNWIND_BREAK) break;
        // For UNWIND_CONTINUE we just continue, the inner Node has already
        // unwound by this point.
        // Still check for other kinds of unwinding (i.e. return, throw):
        if (vm->IsUnwinding()) break;

        delete after->Eval(vm);
        if (vm->IsUnwinding()) break;
    }

    vm->PopLocalFrame();
    return new NothingValue;
}
Node* ForLoop::Clone ()
{
    return new ForLoop(initial->Clone(),cond->Clone(),after->Clone(),block->Clone());
}

ArrBuilder::~ArrBuilder ()
{
    for (Node* node : value_nodes)
    {
        delete node;
    }
}
void ArrBuilder::Append (Node* node)
{
    value_nodes.push_back(node);
}
Value* ArrBuilder::Eval (VM* vm)
{
    ArrValue* arr = new ArrValue(new ArrStorage);
    for (Node* value_node : value_nodes)
    {
        Value* set_to = value_node->Eval(vm);
        if (vm->IsUnwinding())
        {
            delete set_to;
            delete arr;
            return new NothingValue;
        }
        arr->storage->values.push_back(set_to);
    }
    return arr;
}
Node* ArrBuilder::Clone ()
{
    ArrBuilder* out = new ArrBuilder;
    for (Node* node : value_nodes)
    {
        out->Append(node->Clone());
    }
    return out;
}

ObjBuilder::~ObjBuilder ()
{
    for (std::pair<Node*,Node*> kv : kv_nodes)
    {
        delete kv.first;
        delete kv.second;
    }
}
void ObjBuilder::Append (Node* key, Node* value)
{
    kv_nodes.push_back(std::pair<Node*,Node*>(key,value));
}
Value* ObjBuilder::Eval (VM* vm)
{
    ObjValue* obj = new ObjValue(new ObjStorage);
    for (int i = 0; i < kv_nodes.size(); i++)
    {
        std::unique_ptr<StrValue> key ((StrValue*)kv_nodes[i].first->Eval(vm));
        if (vm->IsUnwinding()) goto FAILURE;

        if (key->type != VALUE_STR)
        {
            vm->Throw("Cannot insert non-string key into object");
            goto FAILURE;
        }

        Value* set_to = kv_nodes[i].second->Eval(vm);
        if (vm->IsUnwinding())
        {
            delete set_to;
            goto FAILURE;
        }

        obj->storage->Set(key->str,set_to);
    }
    return obj;

    FAILURE:
    delete obj;
    return new NothingValue;
}
Node* ObjBuilder::Clone ()
{
    ObjBuilder* out = new ObjBuilder;
    for (std::pair<Node*,Node*> kv : kv_nodes)
    {
        out->Append(kv.first->Clone(),kv.second->Clone());
    }
    return out;
}

Return::Return (Node* expr)
{
    this->expr = expr;
}
Return::~Return ()
{
    delete expr;
}
Value* Return::Eval (VM* vm)
{
    vm->Return(expr->Eval(vm)); // if VM starts unwinding, the Return will be
                                // ignored so this is safe.
    return new NothingValue;
}
Node* Return::Clone ()
{
    return new Return(expr->Clone());
}

Value* Break::Eval (VM* vm)
{
    vm->Break();
    return new NothingValue;
}
Node* Break::Clone ()
{
    return new Break;
}

Value* Continue::Eval (VM* vm)
{
    vm->Continue();
    return new NothingValue;
}
Node* Continue::Clone ()
{
    return new Continue;
}

Throw::Throw (Node* expr)
{
    this->expr = expr;
}
Throw::~Throw ()
{
    delete expr;
}
Value* Throw::Eval (VM* vm)
{
    vm->Throw(expr->Eval(vm)); // if VM starts unwinding, the Return will be
                               // ignored so this is safe.
    return new NothingValue;
}
Node* Throw::Clone ()
{
    return new Throw(expr->Clone());
}

TryCatch::TryCatch (Node* try_block, Node* catch_block, Node* catch_lvalue)
{
    this->try_block = try_block;
    this->catch_block = catch_block;
    this->catch_lvalue = catch_lvalue;
}
TryCatch::~TryCatch ()
{
    delete try_block;
    delete catch_block;
    if (catch_lvalue) delete catch_lvalue;
}
Value* TryCatch::Eval (VM* vm)
{
    delete try_block->Eval(vm);
    Value* caught = vm->CaptureThrown();
    if (caught)
    {
        /** Pass caught exception object into catch block.
            This feature is optional, so catch_lvalue may be NULL. **/
        if (catch_lvalue)
        {
            vm->NewLocalFrame();

            Value* prev = catch_lvalue->Set(vm,caught);
            delete caught;
            if (!prev)
            {
                vm->Throw("setting failed (catch lvalue)");
                vm->PopLocalFrame();
                return new NothingValue;
            }
            else delete prev;
        }
        else delete caught;

        delete catch_block->Eval(vm);

        if (catch_lvalue) vm->PopLocalFrame();
    }
}
Node* TryCatch::Clone ()
{
    Node* new_catch_lvalue;
    if (catch_lvalue) new_catch_lvalue = catch_lvalue->Clone();
    else new_catch_lvalue = NULL;
    return new TryCatch(try_block->Clone(),catch_block->Clone(),new_catch_lvalue);
}

VarHold VarRelLoc::GetVar (VM* vm)
{
    VarFrame* frame = vm->current_frame;
    for (int i = 0; i < frames_upward; i++)
        frame = frame->parent_frame;
    return VarHold(frame, id_in_frame);
}
