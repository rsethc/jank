#include "byte-source.h"
#include <stdlib.h>

ByteSource::~ByteSource () {}

BorrowedFileByteSource::BorrowedFileByteSource (FILE* file)
	: file(file)
{}
int BorrowedFileByteSource::ReadByte ()
{
	if (!file) return EOF;
	return fgetc(file);
}

OwnedFileByteSource::OwnedFileByteSource (FILE* file)
	: BorrowedFileByteSource(file)
{}
OwnedFileByteSource::OwnedFileByteSource (char* path)
	: BorrowedFileByteSource(fopen(path,"rb"))
{}
OwnedFileByteSource::~OwnedFileByteSource ()
{
	if (file) fclose(file);
}

BorrowedMemoryByteSource::BorrowedMemoryByteSource (char* buffer, size_t length)
	: buffer(buffer)
	, remaining(length)
{
	if (!buffer) remaining = 0;
}
int BorrowedMemoryByteSource::ReadByte ()
{
	if (!remaining) return EOF;
	remaining--;
	return *buffer++;
}

OwnedMemoryByteSource::OwnedMemoryByteSource (char* buffer, size_t length)
	: BorrowedMemoryByteSource(buffer, length)
	, buffer(buffer)
{}
OwnedMemoryByteSource::~OwnedMemoryByteSource ()
{
	if (buffer) free(buffer);
}
