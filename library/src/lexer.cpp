#include "lexer.h"

#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <vector>
#include <unordered_map>
#include <stdexcept>

Tok::~Tok () {}

SimpleTok::SimpleTok (int type)
{
    this->type = type;
}

IntTok::IntTok (int value)
{
    type = TokType_Int;
    this->value = value;
}

FloatTok::FloatTok (double value)
{
    type = TokType_Float;
    this->value = value;
}

StrTok::StrTok (std::string str)
{
    type = TokType_Str;
    this->str = str;
}

IdentTok::IdentTok (std::string str)
{
    type = TokType_Ident;
    this->str = str;
}

std::unordered_map<std::string, int> Keywords =
{
    {"if",      TokType_If      },
    {"else",    TokType_Else    },
    {"while",   TokType_While   },
    {"for",     TokType_For     },
    {"func",    TokType_Func    },
    {"return",  TokType_Return  },
    {"break",   TokType_Break   },
    {"continue",TokType_Continue},
    {"throw",   TokType_Throw   },
    {"try",     TokType_Try     },
    {"catch",   TokType_Catch   },
};

Lex::Lex (ByteSource* byte_src)
    : byte_src(byte_src)
{
    Advance();
}
void Lex::Advance ()
{
    nextch = byte_src->ReadByte();
}
Lex::~Lex ()
{
    delete byte_src;
}
Tok* Lex::ReadNumeric ()
{
    int integer = 0;

    if (nextch == '.')
    {
        Advance();
        if (!isdigit(nextch)) return new SimpleTok(TokType_Dot);
        goto AFTER_DECIMAL;
    }

    while (isdigit(nextch))
    {
        integer *= 10;
        integer += nextch - '0';
        Advance();
    }

    if (nextch == '.')
    {
        Advance();
        goto AFTER_DECIMAL;
    }

    return new IntTok(integer);



    AFTER_DECIMAL:;

    double floating = integer;
    double power = 1;

    while (isdigit(nextch))
    {
        power *= 0.1;
        floating += power * (nextch - '0');
        Advance();
    }

    return new FloatTok(floating);
}
Tok* Lex::ReadStr ()
{
    Advance(); // discard quote char
    std::string str_content;
    while (nextch != '"' && nextch != EOF)
    {
        str_content.append((char*)&nextch,1);
        Advance();
    }
    if (nextch == '"') Advance(); // discard quote char
    else
    {
        fprintf(stderr,"unterminated string literal\n");
        return new Tok;
    }
    return new StrTok(str_content);
}
Tok* Lex::ReadIdent ()
{
    std::string name;
    while (isalnum(nextch) || nextch == '_')
    {
        name.append((char*)&nextch,1);
        Advance();
    }

    try
    {
        return new SimpleTok(Keywords.at(name));
    }
    catch (std::out_of_range)
    {
        return new IdentTok(name);
    }
}
void Lex::ReadLineComment ()
{
    while (nextch != '\n' && nextch != EOF)
    {
        Advance();
    }
}
void Lex::ReadBlockComment ()
{
    while (true)
    {
        if (nextch == EOF) break;
        if (nextch == '*')
        {
            Advance();
            if (nextch == '/')
            {
                Advance();
                break;
            }
        }
        else Advance();
    }
}
Tok* Lex::ReadTok ()
{
    while (true)
    {
        while (isspace(nextch))
        {
            Advance();
        }
        switch (nextch)
        {
            case EOF:
            return new SimpleTok(TokType_EOF);

            case '0' ... '9':
            case '.':
            return ReadNumeric();

            case 'a' ... 'z':
            case 'A' ... 'Z':
            case '_':
            return ReadIdent();

            case '"':
            return ReadStr();

            case ';':
            Advance();
            return new SimpleTok(TokType_Semicolon);

            case '+':
            Advance();
            switch (nextch)
            {
                case '+':
                Advance();
                return new SimpleTok(TokType_Incr);

                case '=':
                Advance();
                return new SimpleTok(TokType_AddSet);

                default:
                return new SimpleTok(TokType_Add);
            }

            case '-':
            Advance();
            switch (nextch)
            {
                case '-':
                Advance();
                return new SimpleTok(TokType_Decr);

                case '=':
                Advance();
                return new SimpleTok(TokType_MinusSet);

                default:
                return new SimpleTok(TokType_Minus);
            }

            case '*':
            Advance();
            switch (nextch)
            {
                case '=':
                Advance();
                return new SimpleTok(TokType_MulSet);

                default:
                return new SimpleTok(TokType_Mul);
            }

            case '/':
            Advance();
            switch (nextch)
            {
                case '/':
                Advance();
                ReadLineComment();
                continue; // start outer while loop over to get an actual
                          // token before returning

                case '*':
                Advance();
                ReadBlockComment();
                continue;

                case '=':
                Advance();
                return new SimpleTok(TokType_DivSet);

                default:
                return new SimpleTok(TokType_Div);
            }

            case '%':
            Advance();
            switch (nextch)
            {
                case '=':
                Advance();
                return new SimpleTok(TokType_ModulusSet);

                default:
                return new SimpleTok(TokType_Modulus);
            }

            case '=':
            Advance();
            switch (nextch)
            {
                case '=':
                Advance();
                return new SimpleTok(TokType_Equal);

                default:
                return new SimpleTok(TokType_Set);
            }

            case '!':
            Advance();
            switch (nextch)
            {
                case '=':
                Advance();
                return new SimpleTok(TokType_NotEqual);

                default:
                return new SimpleTok(TokType_LogicNot);
            }

            case '>':
            Advance();
            switch (nextch)
            {
                case '=':
                Advance();
                return new SimpleTok(TokType_GreaterEqual);

                case '>':
                Advance();
                return new SimpleTok(TokType_BitRight);

                default:
                return new SimpleTok(TokType_Greater);
            }

            case '<':
            Advance();
            switch (nextch)
            {
                case '=':
                Advance();
                return new SimpleTok(TokType_LessEqual);

                case '<':
                Advance();
                return new SimpleTok(TokType_BitLeft);

                default:
                return new SimpleTok(TokType_Less);
            }

            case '&':
            Advance();
            switch (nextch)
            {
                case '&':
                Advance();
                return new SimpleTok(TokType_LogicAnd);

                default:
                return new SimpleTok(TokType_BitAnd);
            }

            case '|':
            Advance();
            switch (nextch)
            {
                case '|':
                Advance();
                return new SimpleTok(TokType_LogicOr);

                default:
                return new SimpleTok(TokType_BitOr);
            }

            case '^':
            Advance();
            switch (nextch)
            {
                case '^':
                Advance();
                return new SimpleTok(TokType_LogicXor);

                default:
                return new SimpleTok(TokType_BitXor);
            }

            case '(':
            Advance();
            return new SimpleTok(TokType_Open);

            case ')':
            Advance();
            return new SimpleTok(TokType_Close);

            case '{':
            Advance();
            return new SimpleTok(TokType_Start);

            case '}':
            Advance();
            return new SimpleTok(TokType_Finish);

            case '[':
            Advance();
            return new SimpleTok(TokType_ArrStart);

            case ']':
            Advance();
            return new SimpleTok(TokType_ArrFinish);

            case ',':
            Advance();
            return new SimpleTok(TokType_Comma);

            case ':':
            Advance();
            return new SimpleTok(TokType_Colon);

            default:
            Advance(); // discard unknown character
            return new Tok;
        }
    }
}
