#include "parser.h"

Scope::Scope (Scope* parent, bool in_a_loop)
{
    this->parent = parent;
    this->in_a_loop = in_a_loop;
}
Scope::~Scope () {}
bool Scope::LocalLookup (char* name, VarRelLoc* rel_loc)
{
    try
    {
        rel_loc->id_in_frame = declared_vars.at(name);
        return true;
    }
    catch (std::out_of_range)
    {
        return false;
    }
}
bool Scope::RecursiveLookup (char* name, VarRelLoc* rel_loc)
{
    if (LocalLookup(name,rel_loc)) return true;
    if (!parent) return false;
    rel_loc->frames_upward++;
    return parent->RecursiveLookup(name,rel_loc);
}

VarRelLoc Scope::LocalDeclare (char* name)
{
    VarRelLoc rel_loc;
    rel_loc.id_in_frame = declared_vars.size();
    declared_vars[name] = rel_loc.id_in_frame;
    return rel_loc;
}
VarRelLoc Scope::ByName (char* name)
{
    VarRelLoc rel_loc;
    if (RecursiveLookup(name,&rel_loc)) return rel_loc;
    return LocalDeclare(name);
}
VarRelLoc Scope::ByNameLocal (char* name)
{
    VarRelLoc rel_loc;
    if (LocalLookup(name,&rel_loc)) return rel_loc;
    return LocalDeclare(name);
}

Tok* Parse::NextTok ()
{
    if (!next_tok_raw) next_tok_raw = lex->ReadTok();
    return next_tok_raw;
}
Parse::Parse (Lex* lex)
{
    this->lex = lex;
}
void Parse::Advance ()
{
    if (next_tok_raw)
    {
        delete next_tok_raw;
        next_tok_raw = NULL;
    }
}
bool Parse::Expect (int type)
{
    if (NextTok()->type != type) return false;
    Advance();
    return true;
}
Parse::~Parse ()
{
    Advance(); // Deletes current token if it exists.
    delete lex;
    delete global_scope;
}

void Parse::NewScope (bool in_a_loop)
{
    global_scope = new Scope(global_scope, in_a_loop);
}
void Parse::PopScope ()
{
    Scope* exited = global_scope;
    global_scope = exited->parent;
    delete exited;
}

Node* Parse::ReadFuncCall (Node* func_node)
{
    Advance();

    FuncCall* call = new FuncCall(func_node);

    while (true)
    {
        if (Expect(TokType_Close))
        {
            /* end of arg list */
            return call;
        }

        Node* arg = Expr0();
        if (!arg)
        {
            fprintf(stderr,"can't parse args for function call\n");
            delete call;
            delete func_node;
            return NULL;
        }

        call->AddArg(arg);

        if (NextTok()->type == TokType_Close) continue;

        if (!Expect(TokType_Comma))
        {
            fprintf(stderr,"expected comma in function call args list\n");
            delete call;
            delete func_node;
            return NULL;
        }
    }
}
Node* Parse::ReadIndexAccess (Node* arr_node)
{
    Advance();

    Node* idx_expr = Expr0();
    if (!idx_expr)
    {
        fprintf(stderr,"can't parse index\n");
        delete arr_node;
        return NULL;
    }

    if (!Expect(TokType_ArrFinish))
    {
        fprintf(stderr,"closing bracket required after index expression\n");
        delete arr_node;
        delete idx_expr;
        return NULL;
    }

    return new IndexAccess(arr_node,idx_expr);
}
Node* Parse::ReadArrayLiteral ()
{
    Advance();

    ArrBuilder* builder = new ArrBuilder;

    while (true)
    {
        if (Expect(TokType_ArrFinish))
        {
            /* end of element list */
            return builder;
        }

        Node* element = Expr0();
        if (!element)
        {
            fprintf(stderr,"can't parse elements for array literal\n");
            delete builder;
            return NULL;
        }

        builder->Append(element);

        if (NextTok()->type == TokType_ArrFinish) continue;

        if (!Expect(TokType_Comma))
        {
            fprintf(stderr,"expected comma in array literal elements list\n");
            delete builder;
            return NULL;
        }
    }
}
Node* Parse::ReadObjLiteral ()
{
    Advance();

    ObjBuilder* builder = new ObjBuilder;

    while (true)
    {
        if (Expect(TokType_Finish))
        {
            /* end of element list */
            return builder;
        }

        Node* key = Expr0();
        if (!key)
        {
            fprintf(stderr,"can't parse key in object literal\n");
            delete builder;
            return NULL;
        }

        if (!Expect(TokType_Colon))
        {
            fprintf(stderr,"expected ':' after key in object literal\n");
            delete builder;
            delete key;
            return NULL;
        }

        Node* value = Expr0();
        if (!value)
        {
            fprintf(stderr,"can't parse value in object literal\n");
            delete builder;
            delete key;
            return NULL;
        }

        builder->Append(key,value);

        if (NextTok()->type == TokType_Finish) continue;

        if (!Expect(TokType_Comma))
        {
            fprintf(stderr,"expected comma in object literal\n");
            delete builder;
            return NULL;
        }
    }
}
Node* Parse::Leaf ()
{
    switch (NextTok()->type)
    {
        case TokType_Int:
        {
            IntTok* inttok = (IntTok*)NextTok();
            Node* node = new IntConst(inttok->value);
            Advance();
            return node;
        }

        case TokType_Float:
        {
            FloatTok* floattok = (FloatTok*)NextTok();
            Node* node = new FloatConst(floattok->value);
            Advance();
            return node;
        }

        case TokType_Str:
        {
            StrTok* strtok = (StrTok*)NextTok();
            Node* node = new StrConst((char*)strtok->str.c_str());
            Advance();
            return node;
        }

        case TokType_Ident:
        {
            IdentTok* identtok = (IdentTok*)NextTok();
            VarRelLoc var = global_scope->ByName((char*)identtok->str.c_str());
            Advance();
            return new VarAccess(var);
        }

        case TokType_Open:
        {
            Advance();

            if (Expect(TokType_Close))
            {
                // empty parens indicate nothing value
                return new NothingLiteral;
            }

            Node* inner = Expr0();

            if (!inner)
            {
                fprintf(stderr,"can't parse parenthetical expression\n");
                return NULL;
            }

            if (Expect(TokType_Close))
            {
                return inner;
            }
            else
            {
                delete inner;
                fprintf(stderr,"unclosed parentheses\n");
                return NULL;
            }
        }

        case TokType_ArrStart:
        return ReadArrayLiteral();

        case TokType_Start:
        return ReadObjLiteral();

        case TokType_Func:
        return ReadAnonFuncLiteral();

        default:
        fprintf(stderr,"unrecognized token %d\n",NextTok()->type);
        return NULL;
    }
}
Node* Parse::Expr8 (bool allow_func_call)
{
    Node* left = Leaf();
    if (!left)
    {
        return NULL;
    }

    while (true)
    {
        /*  Check if we have parentheses next,
            which would indicate this is a function call. */
        if (NextTok()->type == TokType_Open && allow_func_call)
        {
            left = ReadFuncCall(left);
            if (!left) return NULL;
        }
        else if (NextTok()->type == TokType_ArrStart)
        {
            left = ReadIndexAccess(left);
            if (!left) return NULL;
        }
        else if (NextTok()->type == TokType_Dot)
        {
            Advance();
            if (NextTok()->type != TokType_Ident)
            {
                fprintf(stderr,"Expected identifier after dot\n");
                delete left;
                return NULL;
            }
            left = new DotAccess(left,((IdentTok*)NextTok())->str);
            Advance();
        }
        else
        {
            return left;
        }
    }
}
Node* Parse::Expr7 ()
{
    /* not worrying about loops because ++/-- can't be chained onto each
       other anyway, since "++lvalue" and "lvalue++" are not valid lvalues.
    */

    /* check for prefix operators */
    int optype;
    switch (optype = NextTok()->type)
    {
        case TokType_Incr:
        case TokType_Decr:
        {
            Advance();

            Node* right = Expr8();
            if (!right)
            {
                return NULL;
            }
            Node* changed;
            switch (optype)
            {
                case TokType_Incr:
                changed = new Add(right->Clone(), new IntConst(1));
                break;

                case TokType_Decr:
                changed = new Minus(right->Clone(), new IntConst(1));
                break;
            }
            Node* assignment = new Assign(right, changed);
            return assignment;
        }
    }

    /* prefix operator didn't exist, get inner expr and check for postfix */
    Node* left = Expr8();
    if (!left)
    {
        return NULL;
    }
    switch (optype = NextTok()->type)
    {
        case TokType_Incr:
        case TokType_Decr:
        {
            Advance();

            Node* changed;
            switch (optype)
            {
                case TokType_Incr:
                changed = new Add(left->Clone(), new IntConst(1));
                break;

                case TokType_Decr:
                changed = new Minus(left->Clone(), new IntConst(1));
                break;
            }
            Node* assignment = new PostAssign(left, changed);
            return assignment;
        }
    }

    // no postfix either, return inner expr directly to caller.
    return left;
}
Node* Parse::Expr6 ()
{
    /* check for prefix operators */
    switch (NextTok()->type)
    {
        case TokType_Minus:
        {
            Advance();
            Node* inner = Expr6();
            if (!inner) return NULL;
            return new Negate(inner);
        }

        case TokType_LogicNot:
        {
            Advance();
            Node* inner = Expr6();
            if (!inner) return NULL;
            return new LogicNot(inner);
        }
    }
    return Expr7();
}
Node* Parse::Expr5 ()
{
    Node* left = Expr6();
    if (!left)
    {
        return NULL;
    }

    while (true)
    {
        int optype = NextTok()->type;
        switch (optype)
        {
            default:
            return left;


            case TokType_BitAnd:
            case TokType_BitOr:
            case TokType_BitXor:
            case TokType_BitLeft:
            case TokType_BitRight:

            Advance();

            Node* right = Expr6();

            if (!right)
            {
                delete left;
                return NULL;
            }
            switch (optype)
            {
                case TokType_BitAnd:
                left = new BitAnd(left,right);
                break;

                case TokType_BitOr:
                left = new BitOr(left,right);
                break;

                case TokType_BitXor:
                left = new BitXor(left,right);
                break;

                case TokType_BitLeft:
                left = new BitLeft(left,right);
                break;

                case TokType_BitRight:
                left = new BitRight(left,right);
                break;
            }
        }
    }
}
Node* Parse::Expr4 ()
{
    Node* left = Expr5();
    if (!left)
    {
        return NULL;
    }

    while (true)
    {
        int optype = NextTok()->type;
        switch (optype)
        {
            default:
            return left;


            case TokType_Mul:
            case TokType_Div:
            case TokType_Modulus:

            Advance();

            Node* right = Expr5();

            if (!right)
            {
                delete left;
                return NULL;
            }
            switch (optype)
            {
                case TokType_Mul:
                left = new Mul(left,right);
                break;

                case TokType_Div:
                left = new Div(left,right);
                break;

                case TokType_Modulus:
                left = new Modulus(left,right);
                break;
            }
        }
    }
}
Node* Parse::Expr3 ()
{
    Node* left = Expr4();
    if (!left)
    {
        return NULL;
    }

    while (true)
    {
        int optype = NextTok()->type;
        switch (optype)
        {
            default:
            return left;


            case TokType_Add:
            case TokType_Minus:

            Advance();

            Node* right = Expr4();

            if (!right)
            {
                delete left;
                return NULL;
            }
            switch (optype)
            {
                case TokType_Add:
                left = new Add(left,right);
                break;

                case TokType_Minus:
                left = new Minus(left,right);
                break;
            }
        }
    }
}
Node* Parse::Expr2 ()
{
    Node* left = Expr3();
    if (!left)
    {
        fprintf(stderr,"can't parse left side of + or -\n");
        return NULL;
    }

    while (true)
    {
        int optype = NextTok()->type;
        switch (optype)
        {
            default:
            return left;


            case TokType_LogicAnd:
            case TokType_LogicOr:
            case TokType_LogicXor:

            Advance();

            Node* right = Expr3();

            if (!right)
            {
                return NULL;
            }
            switch (optype)
            {
                case TokType_LogicAnd:
                left = new LogicAnd(left,right);
                break;

                case TokType_LogicOr:
                left = new LogicOr(left,right);
                break;

                case TokType_LogicXor:
                left = new LogicXor(left,right);
                break;
            }
        }
    }
}
Node* Parse::Expr1 ()
{
    Node* left = Expr2();
    if (!left)
    {
        return NULL;
    }

    while (true)
    {
        int optype = NextTok()->type;
        switch (optype)
        {
            default:
            return left;


            case TokType_Equal:
            case TokType_NotEqual:
            case TokType_Greater:
            case TokType_GreaterEqual:
            case TokType_Less:
            case TokType_LessEqual:

            Advance();

            Node* right = Expr2();

            if (!right)
            {
                return NULL;
            }
            switch (optype)
            {
                case TokType_Equal:
                left = new Equal(left,right);
                break;

                case TokType_NotEqual:
                left = new NotEqual(left,right);
                break;

                case TokType_Greater:
                left = new Greater(left,right);
                break;

                case TokType_GreaterEqual:
                left = new GreaterEqual(left,right);
                break;

                case TokType_Less:
                left = new Less(left,right);
                break;

                case TokType_LessEqual:
                left = new LessEqual(left,right);
                break;
            }
        }
    }
}
Node* Parse::Expr0 ()
{
    Node* left = Expr1();
    if (!left)
    {
        return NULL;
    }

    int optype = NextTok()->type;
    switch (optype)
    {
        default:
        return left;


        case TokType_Set:
        case TokType_AddSet:
        case TokType_MinusSet:
        case TokType_MulSet:
        case TokType_DivSet:
        case TokType_ModulusSet:

        Advance();

        Node* right = Expr0();
        if (!right)
        {
            delete left;
            return NULL;
        }

        switch (optype)
        {
            case TokType_AddSet:
            right = new Add(left->Clone(), right);
            break;

            case TokType_MinusSet:
            right = new Minus(left->Clone(), right);
            break;

            case TokType_MulSet:
            right = new Mul(left->Clone(), right);
            break;

            case TokType_DivSet:
            right = new Div(left->Clone(), right);
            break;

            case TokType_ModulusSet:
            right = new Modulus(left->Clone(), right);
            break;
        }

        Node* assignment = new Assign(left,right);
        return assignment;
    }
}
Node* Parse::ReadIfWhile (bool is_while)
{
    Advance();

    if (!Expect(TokType_Open))
    {
        fprintf(stderr,"parentheses must be opened after if keyword\n");
        return NULL;
    }

    NewScope(is_while || global_scope->in_a_loop);

    Node* cond;
    if (NextTok()->type == TokType_Close) cond = new NothingLiteral;
    else
    {
        cond = Expr0();
        if (!cond)
        {
            fprintf(stderr,"can't parse if condition\n");
            PopScope();
            return NULL;
        }
    }

    if (!Expect(TokType_Close))
    {
        fprintf(stderr,"parentheses must be closed after if condition\n");
        delete cond;
        PopScope();
        return NULL;
    }

    Node* stmt;
    if (Expect(TokType_Semicolon))
    {
        stmt = new NothingLiteral;
    }
    else
    {
        stmt = ReadStmt();
        if (!stmt)
        {
            fprintf(stderr,"can't parse if statement body\n");
            delete cond;
            PopScope();
            return NULL;
        }
    }

    PopScope();
    if (is_while) return new WhileStmt(cond,stmt);
    else
    {
        /* Also check for else-if's & else. */
        IfStmt* if_stmt = new IfStmt;
        if_stmt->pairs.push_back(IfPair(cond,stmt));

        ANOTHER_ELSEIF:
        if (Expect(TokType_Else))
        {
            if (Expect(TokType_If))
            {
                /* Else-if */
                if (!Expect(TokType_Open))
                {
                    fprintf(stderr,"parentheses expected for else-if condition");
                    delete if_stmt;
                    return NULL;
                }

                NewScope(global_scope->in_a_loop);



                Node* cond;
                if (NextTok()->type == TokType_Close) cond = new NothingLiteral;
                else
                {
                    cond = Expr0();
                    if (!cond)
                    {
                        fprintf(stderr,"can't parse else-if condition\n");
                        PopScope();
                        delete if_stmt;
                        return NULL;
                    }
                }

                if (!Expect(TokType_Close))
                {
                    fprintf(stderr,"parentheses must be closed after else-if condition\n");
                    delete cond;
                    PopScope();
                    delete if_stmt;
                    return NULL;
                }

                Node* stmt;
                if (Expect(TokType_Semicolon))
                {
                    stmt = new NothingLiteral;
                }
                else
                {
                    stmt = ReadStmt();
                    if (!stmt)
                    {
                        fprintf(stderr,"can't parse else-if statement body\n");
                        delete cond;
                        PopScope();
                        delete if_stmt;
                        return NULL;
                    }
                }



                PopScope();
                if_stmt->pairs.push_back(IfPair(cond,stmt));
                goto ANOTHER_ELSEIF;
            }
            else
            {
                /* Else */
                // For now an unconditional else is just another IfPair with a
                // condition that'll always evaluate to true.

                NewScope(global_scope->in_a_loop);

                Node* stmt;
                if (Expect(TokType_Semicolon))
                {
                    stmt = new NothingLiteral;
                }
                else
                {
                    stmt = ReadStmt();
                    if (!stmt)
                    {
                        fprintf(stderr,"can't parse else-if statement body\n");
                        delete cond;
                        PopScope();
                        delete if_stmt;
                        return NULL;
                    }
                }

                PopScope();
                if_stmt->pairs.push_back(IfPair(new IntConst(1),stmt));
            }
        }

        return if_stmt;
    }
}
Node* Parse::ReadFor ()
{
    Advance();

    if (!Expect(TokType_Open))
    {
        fprintf(stderr,"parentheses must be opened after for keyword\n");
        return NULL;
    }

    NewScope(true);

    Node* initial;
    if (NextTok()->type == TokType_Semicolon) initial = new NothingLiteral;
    else
    {
        initial = Expr0();
        if (!initial)
        {
            fprintf(stderr,"can't parse for initial statement\n");
            PopScope();
            return NULL;
        }
    }

    if (!Expect(TokType_Semicolon))
    {
        fprintf(stderr,"semicolon required after for initial\n");
        delete initial;
        PopScope();
        return NULL;
    }

    // Truthy values as a default when no condition is specified,
    // so that "for (;;) " is an infinite loop as it is in C.
    Node* cond;
    if (NextTok()->type == TokType_Semicolon) cond = new IntConst(1);
    else
    {
        cond = Expr0();
        if (!cond)
        {
            fprintf(stderr,"can't parse for cond statement\n");
            delete initial;
            PopScope();
            return NULL;
        }
    }

    if (!Expect(TokType_Semicolon))
    {
        fprintf(stderr,"semicolon required after for cond\n");
        delete initial;
        delete cond;
        PopScope();
        return NULL;
    }

    Node* after;
    if (NextTok()->type == TokType_Close) after = new IntConst(1);
    else
    {
        after = Expr0();
        if (!after)
        {
            fprintf(stderr,"can't parse for after statement\n");
            delete initial;
            delete cond;
            PopScope();
            return NULL;
        }
    }

    if (!Expect(TokType_Close))
    {
        fprintf(stderr,"parentheses must be closed after for after\n");
        delete initial;
        delete cond;
        delete after;
        PopScope();
        return NULL;
    }

    Node* stmt;
    if (Expect(TokType_Semicolon))
    {
        stmt = new NothingLiteral;
    }
    else
    {
        stmt = ReadStmt();
        if (!stmt)
        {
            fprintf(stderr,"can't parse for loop body\n");
            delete initial;
            delete cond;
            delete after;
            PopScope();
            return NULL;
        }
    }

    PopScope();
    return new ForLoop(initial,cond,after,stmt);
}
Node* Parse::ReadBlock ()
{
    Advance();

    Block* block = new Block;
    NewScope(global_scope->in_a_loop);

    while (true)
    {
        DiscardSemicolons();

        if (Expect(TokType_Finish))
        {
            PopScope();
            return block;
        }

        Node* stmt = ReadStmt();
        if (!stmt)
        {
            fprintf(stderr,"can't read next statement in block\n");
            delete block;
            PopScope();
            return NULL;
        }
        block->Append(stmt);
    }
}
void Parse::DiscardSemicolons ()
{
    while (Expect(TokType_Semicolon));
}

Node* Parse::ReadScriptFuncDefinition ()
{
    // ( ) optional, not required if there are no arguments at all
    std::vector<Tok*> argnames;
    if (Expect(TokType_Open))
    {
        while (NextTok()->type != TokType_Close)
        {
            if (NextTok()->type != TokType_Ident)
            {
                fprintf(stderr,"func arg name isn't a valid identifier\n");
                for (Tok* argname : argnames) delete argname;
                return NULL;
            }
            argnames.push_back(NextTok());

            /** ADVANCE WOULD DELETE THE TOKEN **/
            next_tok_raw = NULL;
            Advance();

            if (NextTok()->type == TokType_Close) break;

            if (!Expect(TokType_Comma))
            {
                fprintf(stderr,"func arg names must be comma separated\n");
                for (Tok* argname : argnames) delete argname;
                return NULL;
            }
        }
        // get rid of closing paren
        Advance();
    }

    if (NextTok()->type != TokType_Start)
    {
        fprintf(stderr,"expected { for function code\n");
        for (Tok* argname : argnames) delete argname;
        return NULL;
    }

    NewScope(false);

    for (Tok* argname : argnames)
    {
        IdentTok* ident = (IdentTok*)argname;
        global_scope->ByNameLocal((char*)ident->str.c_str());
        delete argname;
    }

    func_impl_depth++;
    Node* body = ReadBlock();
    func_impl_depth--;

    PopScope();

    if (!body)
    {
        fprintf(stderr,"can't read func impl\n");
        return NULL;
    }

    return new FuncLiteralNode(body, argnames.size());
}

Node* Parse::ReadNamedScriptFunc ()
{
    Advance();

    Node* name = Expr8(false);
    if (!name)
    {
        fprintf(stderr,"can't read func decl\n");
        return NULL;
    }

    Node* func_literal = ReadScriptFuncDefinition();
    if (!func_literal)
    {
        delete name;
        return NULL;
    }

    Node* assign = new Assign(name, func_literal);
    return assign;
}

Node* Parse::ReadAnonFuncLiteral ()
{
    Advance();
    return ReadScriptFuncDefinition();
}

Node* Parse::ReadReturn ()
{
    Advance();

    if (!func_impl_depth)
    {
        fprintf(stderr,"return statement outside of any function\n");
        return NULL;
    }

    Node* expr;
    switch (NextTok()->type)
    {
        case TokType_Semicolon:
        case TokType_Finish:
        expr = new NothingLiteral();
        break;

        default:
        expr = Expr0();
        if (!expr) return NULL;
    }

    if (NextTok()->type != TokType_Semicolon && !SEMICOLONS_OPTIONAL)
    {
        fprintf(stderr,"missing semicolon\n");
        delete expr;
        return NULL;
    }
    DiscardSemicolons();

    return new Return(expr);
}
Node* Parse::ReadBreakContinue (bool is_continue)
{
    Advance();
    /*  TO DO: See if there is an identifier here
        to specify a particular loop.  */

    if (!global_scope->in_a_loop)
    {
        fprintf(stderr,"break/continue statement not in a loop\n");
        return NULL;
    }

    if (is_continue) return new Continue;
    else return new Break;
}
Node* Parse::ReadThrow ()
{
    Advance();

    Node* thrown_expr = Expr0();
    if (!thrown_expr)
    {
        fprintf(stderr,"Can't parse thrown expression.\n");
        return NULL;
    }

    return new Throw(thrown_expr);
}
Node* Parse::ReadTryCatch ()
{
    Advance();

    Node* try_block = ReadStmt();
    if (!try_block)
    {
        fprintf(stderr,"Can't parse try block.\n");
        return NULL;
    }

    if (!Expect(TokType_Catch))
    {
        fprintf(stderr,"Expected 'catch' after try block.\n");
        delete try_block;
        return NULL;
    }

    Node* catch_lvalue;
    if (Expect(TokType_Open))
    {
        // optional. In this case we *do* have an lvalue to store the
        // thrown exception in.

        NewScope(global_scope->in_a_loop);

        catch_lvalue = Expr0();
        if (!catch_lvalue)
        {
            fprintf(stderr,"Can't parse lvalue for thrown exception");
            delete try_block;
            PopScope();
            return NULL;
        }

        if (!Expect(TokType_Close))
        {
            fprintf(stderr,"Expected ')' after thrown exception lvalue.\n");
            delete try_block;
            delete catch_lvalue;
            PopScope();
            return NULL;
        }
    }
    else catch_lvalue = NULL;

    Node* catch_block = ReadStmt();
    if (!catch_block)
    {
        fprintf(stderr,"Can't parse catch block.\n");
        delete try_block;
        if (catch_lvalue)
        {
            delete catch_lvalue;
            PopScope();
        }
        return NULL;
    }

    if (catch_lvalue) PopScope();
    return new TryCatch(try_block,catch_block,catch_lvalue);
}
Node* Parse::ReadStmt ()
{
    DiscardSemicolons();

    switch (NextTok()->type)
    {
        case TokType_EOF:
        return NULL;

        case TokType_If:
        return ReadIfWhile(false);

        case TokType_While:
        return ReadIfWhile(true);

        case TokType_For:
        return ReadFor();

        case TokType_Start:
        return ReadBlock();

        // Func starting a line will expect an lvalue after the 'func' keyword,
        // where the function will be assigned to, and semicolon not required.
        case TokType_Func:
        return ReadNamedScriptFunc();

        case TokType_Return:
        return ReadReturn();

        case TokType_Break:
        return ReadBreakContinue(false);

        case TokType_Continue:
        return ReadBreakContinue(true);

        case TokType_Throw:
        return ReadThrow();

        case TokType_Try:
        return ReadTryCatch();

        default:
        {
            Node* expr = Expr0();

            if (NextTok()->type == TokType_Semicolon) Advance();
            else if (!SEMICOLONS_OPTIONAL)
            {
                fprintf(stderr,"missing semicolon\n");
                delete expr;
                return NULL;
            }

            return expr;
        }
    }
}
