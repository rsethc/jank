/*
    Code in this file is adapted from:
        https://stackoverflow.com/a/4793662/1917534

    License info:
        https://creativecommons.org/licenses/by-sa/4.0/

    Changes made to this from the original:
        Separated the sample code into header and source files.
        Applied superficial code styling changes.
        Implemented acquire/release of arbitrary amounts instead of always 1.
        Constructor for specifying an arbitrary initial count.

    This file is licensed under the license linked in
        this comment, rather than the MIT license in this repository.
*/

#include "sem-polyfill.h"

semaphore::semaphore (int initial)
{
    count_ = initial;
}

void semaphore::release ()
{
    std::lock_guard<decltype(mutex_)> lock(mutex_);
    ++count_;
    condition_.notify_one();
}

void semaphore::acquire ()
{
    std::unique_lock<decltype(mutex_)> lock(mutex_);
    while (!count_) // Handle spurious wake-ups.
        condition_.wait(lock);
    --count_;
}

bool semaphore::try_acquire ()
{
    std::lock_guard<decltype(mutex_)> lock(mutex_);
    if (count_)
    {
        --count_;
        return true;
    }
    return false;
}

void semaphore_amounts::release (int release_amount)
{
    std::lock_guard<decltype(mutex_)> lock(mutex_);

    count_ += release_amount;

    // Even if we only released one, it's possible notify_one would wake a
    // thread that is waiting on 2 but not wake another thread that is waiting
    // on 1, leading to a potential deadlock even though the caller uses the
    // sems in a correct way. So always notify_all if we allow arbitrary amount.
    condition_.notify_all();
}

void semaphore_amounts::acquire (int acquire_amount)
{
    std::unique_lock<decltype(mutex_)> lock(mutex_);

    // Handle spurious wake-ups by checking count and waiting again if needed.
    while (count_ < acquire_amount)
        condition_.wait(lock);

    count_ -= acquire_amount;
}

bool semaphore_amounts::try_acquire (int acquire_amount)
{
    std::lock_guard<decltype(mutex_)> lock(mutex_);

    if (count_ < acquire_amount)
        return false;

    count_ -= acquire_amount;
    return true;
}
