#include <stdarg.h>
#include <stdio.h>
#include "console-colors.h"

#ifdef _WIN32

#include <windows.h>
WORD ChangeForegroundFlags (WORD attr, WORD foreground)
{
    const WORD mask = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY;
    return (attr & ~mask) | foreground;
}
WORD GetConsoleAttr (DWORD which_output)
{
    HANDLE console = GetStdHandle(which_output);
    if (console && console != INVALID_HANDLE_VALUE)
    {
        CONSOLE_SCREEN_BUFFER_INFO console_info;
        if (GetConsoleScreenBufferInfo(console, &console_info))
        {
            return console_info.wAttributes;
        }
    }
    return FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;
}
void SetConsoleAttr (DWORD which_output, WORD attr)
{
    HANDLE console = GetStdHandle(which_output);
    if (console && console != INVALID_HANDLE_VALUE)
    {
        SetConsoleTextAttribute(console, attr);
    }
}

void err_printf (const char* format, ...)
{
    va_list args;
    va_start(args, format);

    WORD before = GetConsoleAttr(STD_ERROR_HANDLE);
    SetConsoleAttr(STD_ERROR_HANDLE,
                   ChangeForegroundFlags(before,
                                         FOREGROUND_RED));

    vfprintf(stderr, format, args);

    SetConsoleAttr(STD_ERROR_HANDLE, before);

    va_end(args);
}

#else // Assume Linux

void err_printf (const char* format, ...)
{

    va_list args;
    va_start(args, format);

    fprintf(stderr, "\033[31m");

    vfprintf(stderr, format, args);

    fprintf(stderr, "\033[0m");

    va_end(args);
}

#endif
