#include <jank/lexer.h>
#include <jank/parser.h>
#include <jank/vm.h>
#include <jank/jankstdlib.h>
#include <mutex>
#include <unistd.h>

#include "console-colors.h"

extern std::mutex print_mutex;

class InteractiveStdin : public BorrowedFileByteSource
{
    bool was_newline = true;
public:
    InteractiveStdin ()
        : BorrowedFileByteSource(stdin)
    {}
    int ReadByte () override
    {
        if (was_newline) err_printf(">>> ");
        int byte = BorrowedFileByteSource::ReadByte();
        was_newline = byte == '\n';
        return byte;
    }
};

int main (int argc, char** argv)
{
    bool script_stdin = argc < 2;
    ByteSource* script_src;
    if (script_stdin)
    {
        if (isatty(fileno(stdin)))
            script_src = new InteractiveStdin;
        else
            script_src = new BorrowedFileByteSource(stdin);
    }
    else
    {
        char* script_path = argv[1];
        FILE* script_file = fopen(script_path,"rb");
        if (!script_file)
        {
            err_printf("Can't open script from \"%s\" for reading.",script_path);
            return 1;
        }
        script_src = new OwnedFileByteSource(script_file);
    }

    Lex* lex = new Lex(script_src);
    Parse* parse = new Parse(lex);
    VM* vm = new VM;
    AddStandardLibrary(parse, vm);

    READ_MORE:

    while (Node* stmt = parse->ReadStmt())
    {
        delete stmt->Eval(vm);
        delete stmt;
        Value* thrown = vm->CaptureThrown();
        if (thrown)
        {
            std::lock_guard<decltype(print_mutex)> lock(print_mutex);
            err_printf(
                "Script threw an exception:\n\"%s\"\n",
                thrown->ToString().c_str()
            );
            delete thrown;
            break;
        }
    }

    if (script_stdin && isatty(fileno(stdin)))
    {
        lex->ReadLineComment();
        parse->Advance();
        goto READ_MORE;
    }

    // Parse and VM are separate, but Parse frees Lex, and Lex frees ByteSrc.
    delete parse;
    delete vm;

    return 0;
}
